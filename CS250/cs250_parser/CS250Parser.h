/*!
******************************************************************************
\file    CS250Parser.h
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    11-02-2020

\brief
Parser Class to read from input.txt

******************************************************************************/
#pragma once

#include <vector>
#include "../Extras/Point4.h"

class CS250Parser
{
	public:
		static void LoadDataFromFile();

		struct Face
		{
			int indices[3];
		};

		static float left;
		static float right;
		static float top;
		static float bottom;
		static float focal;
		static float nearPlane;
		static float farPlane;

		static std::vector<Point4> vertices;//8 edges
		static std::vector<Face> faces;//12 triangles
		static std::vector<Point4> colors;//12 colors (1 per point, 36)
		static std::vector<Point4> textureCoords;//36 coordinates, 3 per triangle

};