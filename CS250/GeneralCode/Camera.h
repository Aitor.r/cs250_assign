/*!
******************************************************************************
\file    Camera.h
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 3
\date    26-02-2020

\brief
.h for the Camera variables and functions

******************************************************************************/
#pragma once

#include "../Extras/Matrix4.h"
#include "Classes.h"

#define InitD 115.0f
#define InitH 20.0f

struct Camera {
public:
	Camera();

	Vector4 ViewVec;
	Vector4 UpVec;
	Vector4 Right;
	Point4 CoP;

	Matrix4 GeneralRot;

	Point4 NearTL;
	Point4 NearTR;
	Point4 NearBL;
	Point4 NearBR;
	Point4 FarTL;
	Point4 FarTR;
	Point4 FarBL;
	Point4 FarBR;

	Vector4 makePlane(Point4 P1, Point4 P2, Point4 P3);
	void PointSet();


	float distance = InitD;
	float height = InitH;

	void GeneralGen();

	void Create();
	void Create(TankB whereto);
	void CreateF(TankB wherefro);
	void Update();
	void Update(TankB whereto);
	void UpdateF(TankB wherefro);
};