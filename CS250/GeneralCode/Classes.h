/*!
******************************************************************************
\file    Classes.h
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
 The classes for the tank pieces

******************************************************************************/
#pragma once
#include "../Extras/Matrix4.h"
#include "Transitions.h"
#include <vector>
#include "..\cs250_rasterizer\Rasterizer.h"

//Class for the Tank Body, with its scale, rotation and the axis, position... Base for others
struct TankB {
	public:
		TankB();//default constructor 

		Point4 Scal;
		Point4 Pos;
		float Rot;
		//Rotation values for each axis
		float Rotu=0.0f;
		float Rotf=0.0f;
		float Rotr=0.0f;

		//The three axises
		Vector4 For;
		Vector4 Up;
		Vector4 Right;

		//Rotation matrix for each axis
		Matrix4 RotUp;
		Matrix4 RotF;
		Matrix4 RotR;

		//Matrices to use in SRT concatenation
		Matrix4 Rota;
		Matrix4 Tras;
		Matrix4 Sca;

		//Storing; vertices, the faces post-clip, colors post-clip, back-face post-clip, Cull post-clip
		std::vector<Point4> vertices;
		std::vector<Point4> verticai;
		std::vector<FACE> newF;
		std::vector<Point4> Colo;
		std::vector<bool> faceoff;
		std::vector<bool> Canwe;

		//Calculates one normal per face
		std::vector<Vector4> Normal(const std::vector<Point4> vertices);
		//Clips
		void Clippo(const std::vector<bool> faces, Transitions FromTo);

		//Calculate General rotation
		void Update();
		void UpdateF();//Rotate around Forward vector
		void UpdateU();//Around Up vector
		void UpdateR();//Around Right vector
};

//Class for the other plane parts
struct Blocky {
public:
	Blocky();//constructor 
	Blocky(Point4 Posi, Point4 Scali);//custom

	//Scale, rotation, position, and respective matrices
	Point4 Scal;
	Point4 Pos;
	float Rot;
	Vector4 RotAx;

	Matrix4 Rota;
	Matrix4 Tras;
	Matrix4 Sca;

	//A concatenation to combine them all
	Matrix4 Conc;

	//Will hold if every vertex is drawable or not
	std::vector<bool> CanI;

	//Storing; vertices, the faces post-clip, colors post-clip, back-face post-clip, Cull post-clip
	std::vector<Point4> vertice;
	std::vector<Point4> verticai;
	std::vector<FACE> newF;
	std::vector<Point4> Colo;
	std::vector<bool> faceoff;

	//Update, a multiplication by general rotation matrix and a function to draw the cube
	void Uppo();
	void Generat(Matrix4 Cam);

	//Creates normal per face
	std::vector<Vector4> Normal(const std::vector<Point4> vertices);

	//Clips
	void Clippo(const std::vector<bool> faces, Transitions FromTo);
	//All the vertices for the cube

	TankB *Dad;
};

