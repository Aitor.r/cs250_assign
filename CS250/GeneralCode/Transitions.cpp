/*!
******************************************************************************
\file    Transitions.cpp
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  Definitions of the functions from the .h

******************************************************************************/
#include "Transitions.h"
#include "../cs250_parser/CS250Parser.h"//CS250::Parser
#include "FrameBuffer.h"

//Will Set the Matrices to the appropiate transformations when constructed
Transitions::Transitions() {
	float WWidth = CS250Parser::right - CS250Parser::left;//Calculate Width of the Camera
	float WHwight = CS250Parser::top - CS250Parser::bottom;//Calculate Height of the Camera

	ViewPort.Identity();//Initialize Viewport matrix to Identity (Less writing)
	ViewPort.m[0][0] = Thicc / 2; ViewPort.m[0][3] = Thicc / 2;//First row must transform the x accordingly  / We must divide by 2 because the range is now from -1 to 1
	ViewPort.m[1][1] = -Slicc / 2; ViewPort.m[1][3] = Slicc / 2;//Second row must transform the y accordingly/ We must divide by 2 because the range is now from -1 to 1

	Proj.Identity();//Initialize Projection plane matrix to Identity (Less writing)

	//Set all the different projection values
	Proj.m[0][0] = (2 * CS250Parser::focal / WWidth);
	Proj.m[1][1] = (2 * CS250Parser::focal / WHwight);
	Proj.m[2][2] = -(CS250Parser::farPlane + CS250Parser::nearPlane) / (CS250Parser::farPlane - CS250Parser::nearPlane);
	Proj.m[2][3] = (-2 * CS250Parser::nearPlane*CS250Parser::farPlane) / (CS250Parser::farPlane - CS250Parser::nearPlane);
	Proj.m[3][2] = -1.0f;
	Proj.m[3][3] = 0.0f;


}

//Updates the Given Matrix with the Transformation matrices
Point4 Transitions::Update(Point4 * Mat) {
	*Mat = Proj*(*Mat);

	for (unsigned int i = 0; i < sizeof(*Mat); i++)
		Mat->v[i] /= Mat->w;

	*Mat = ViewPort*(*Mat);

	return (*Mat);
}

void Transitions::Combine(Matrix4 cam) {
	Comb = Proj * cam;

	Top = Point4(Comb.m[1][0], Comb.m[1][1], Comb.m[1][2], Comb.m[1][3]) - Point4(Comb.m[3][0], Comb.m[3][1], Comb.m[3][2], Comb.m[3][3]);
	Bot = -Point4(Comb.m[1][0], Comb.m[1][1], Comb.m[1][2], Comb.m[1][3]) - Point4(Comb.m[3][0], Comb.m[3][1], Comb.m[3][2], Comb.m[3][3]);
	Right = Point4(Comb.m[0][0], Comb.m[0][1], Comb.m[0][2], Comb.m[0][3]) - Point4(Comb.m[3][0], Comb.m[3][1], Comb.m[3][2], Comb.m[3][3]);
	Left = -Point4(Comb.m[0][0], Comb.m[0][1], Comb.m[0][2], Comb.m[0][3]) - Point4(Comb.m[3][0], Comb.m[3][1], Comb.m[3][2], Comb.m[3][3]);
	Near = -Point4(Comb.m[2][0], Comb.m[2][1], Comb.m[2][2], Comb.m[2][3]) - Point4(Comb.m[3][0], Comb.m[3][1], Comb.m[3][2], Comb.m[3][3]);

}