/*!
******************************************************************************
\file    FrameBuffer.h
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  A class that will act as a "Framebuffer"

******************************************************************************/
#pragma once
#include <SFML/Graphics.hpp>
#define Thicc 960//Defines not to use magic numbers. Width
#define Slicc 720//Defines not to use magic numbers. Height

class FrameBuffer {
	public:
		//Initialize
		void Initialize();

		//I will borrow your union for the depth buffer
		union
		{
			float m[Thicc][Slicc];
			float v[Thicc*Slicc];
		};

		//Instance creator
		static FrameBuffer* Instance() {
			if (!instance) {
				instance = new FrameBuffer;
				instance->Initialize();
			}
			return instance;
		}

		//(Updated for depth buffering)
		void SetPixel(unsigned int x, unsigned int y, float z, sf::Color col);

		sf::RenderWindow window;//SFML a window. 
		sf::Image image;		//SFML what to put in texture. 
		sf::Texture texture;	//SFML what to put in image. 
		sf::Sprite sprite;		//SFML what to put in window. 

	private:
		static FrameBuffer * instance;
};
//Instance
#define FB (FrameBuffer::Instance())