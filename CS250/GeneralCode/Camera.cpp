/*!
******************************************************************************
\file    Camera.cpp
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 3
\date    26-02-2020

\brief
Function definitions of the camera

******************************************************************************/
#include "Camera.h"
#include <math.h>
#include <iostream>
#include "../cs250_parser/CS250Parser.h"
#include "FrameBuffer.h"


//Calculates the 8 ponts that make up the ViewFustrum
void Camera::PointSet() {
	//Field of view calculated
	float FoV = tan(CS250Parser::focal / Thicc);

	//Width and height of near and far planes
	float wN = 2 * tan(FoV)*CS250Parser::nearPlane;
	float hN = wN / (Thicc / Slicc);
	float wF = 2 * tan(FoV)*CS250Parser::farPlane;
	float hF = wF / (Thicc / Slicc);

	Point4 CN = CoP + ViewVec * CS250Parser::nearPlane;
	Point4 CF = CoP + ViewVec * CS250Parser::farPlane;

	NearTL = CN + (UpVec*hN / 2) - (Right*wN / 2);
	NearTR = CN + (UpVec*hN / 2) + (Right*wN / 2);
	NearBL = CN - (UpVec*hN / 2) - (Right*wN / 2);
	NearBR = CN - (UpVec*hN / 2) + (Right*wN / 2);
}

//Basic Constructor
Camera::Camera() {
	UpVec = Vector4(0.0f, 1.0f, 0.0f);//Pointing up
	UpVec.Normalize();
	ViewVec = Vector4(0.0f, 0.0f, -1.0f);//Looking z axis
	ViewVec.Normalize();
	Right = ViewVec.Cross(UpVec);//Right vector Perpendicular to the other 2
	Right.Normalize();
	CoP = Point4();//Center at 0,0
}

//Resets the camera to the fixed values, same as constructor
void Camera::Create() {
	UpVec = Vector4(0.0f, 1.0f, 0.0f);
	UpVec.Normalize();
	ViewVec = Vector4(0.0f, 0.0f, -1.0f);
	ViewVec.Normalize();
	Right = ViewVec.Cross(UpVec);
	Right.Normalize();
	CoP = Point4();

	PointSet();

}



//Fixed Update doesn't need nothing
void Camera::Update() {
}

//Third Person camera initialize
void Camera::Create(TankB whereto) {
	//We need an upvector
	UpVec = Vector4(0.0f, 1.0f, 0.0f);

	//Normalized vector of where we need to look at
	Vector4 TankUp = whereto.Up;
	TankUp.Normalize();
	//The direction the body behind of which we are, normalized
	Vector4 Tankfor = whereto.For;
	Tankfor.Normalize();

	//Calculation of the Original center of projection
	CoP = whereto.Pos - Tankfor*(distance) + TankUp*(height);
	//Calculation of the view vector(where are looking at)
	ViewVec = (whereto.Pos - CoP);
	ViewVec.Normalize();
	//calculation of the right vector(Perpen to view and y axis)
	Right = ViewVec.Cross(UpVec);

	//Recalculation of upvector
	UpVec = Right.Cross(ViewVec);
	UpVec.Normalize();

	PointSet();
}
//Third Person camera update
void Camera::Update(TankB whereto) {
	UpVec = Vector4(0.0f, 1.0f, 0.0f);
	//Normalized vector of where we need to look at
	Vector4 TankUp = whereto.Up;
	TankUp.Normalize();
	//The direction the body behind of which we are, normalized
	Vector4 Tankfor = whereto.For;
	Tankfor.Normalize();

	//Recalculations
	CoP = (whereto.Pos) - Tankfor*(distance) + TankUp*(height);
	ViewVec = (whereto.Pos - CoP);
	ViewVec.Normalize();
	Right = ViewVec.Cross(UpVec);
	Right.Normalize();
	//RERecalculation
	UpVec = Right.Cross(ViewVec);
	UpVec.Normalize();

	PointSet();
}

//First Person Camera creation
void Camera::CreateF(TankB wherefro) {
	//Pointing up
	UpVec = wherefro.Up;
	UpVec.Normalize();
	//Where are we pointing towards 
	ViewVec = wherefro.For;
	ViewVec.Normalize();

	//Right vector calculations
	Right = ViewVec.Cross(UpVec);

	//Where the camera is at the beginning
	CoP = wherefro.Pos;

	PointSet();
}

//First Person Camera Update
void Camera::UpdateF(TankB wherefro) {

	//Recalculate the orientation
	UpVec = wherefro.Up;
	ViewVec = wherefro.For;
	ViewVec.Normalize();

	//Right vector again
	Right = ViewVec.Cross(UpVec);
	//Where is the camera right now
	CoP = wherefro.Pos;

	PointSet();
}

void Camera::GeneralGen() {
	//We translate to the new center of the world(camera)
	Matrix4 Step1;
	Step1.Identity();
	Step1.m[0][3] = -CoP.x; Step1.m[1][3] = -CoP.y; Step1.m[2][3] = -CoP.z;

	//Define the 3 Vectors to use in the transformation
	Vector4 U = -ViewVec; //I thought It was supposed to towards -z axis. I will need thorough feedback on this part
	Vector4 V = -(U.Cross(UpVec)); 
	Vector4 W = UpVec;
	
	//Transformation vector part
	Matrix4 T = Matrix4(V.x, V.y, V.z, 0.0f,
						W.x, W.y, W.z, 0.0f,
						U.x, U.y, U.z, 0.0f,
						0.0f, 0.0f, 0.0f, 1.0f);
	/*
	Matrix4 TT = Matrix4(V.x, W.x, U.x, CoP.x,
						 V.y, W.y, U.y, CoP.y,
						 V.z, W.z, U.z, CoP.z,
						 0.0f, 0.0f, 0.0f, 1.0f);
	*/
	//Final transformation is vector part*translation part
	GeneralRot = (T*Step1);


	//float Root = sqrtf(ViewVec.z*ViewVec.z + ViewVec.x*ViewVec.x);
	////Rotation around y axis to align with the z axis
	//Matrix4 Step2;
	//Step2.Identity();
	//Step2.m[0][0] = -ViewVec.z / Root; Step2.m[0][2] = ViewVec.x / Root;
	//Step2.m[2][0] = -ViewVec.x / Root; Step2.m[2][2] = -ViewVec.z / Root;
	//
	////Rotation around x axis to align with y axis
	//Matrix4 Step3;
	//Step3.Identity();
	//Step3.m[1][1] = Root / ViewVec.Length(); Step3.m[1][2] = ViewVec.y / ViewVec.Length();
	//Step3.m[2][1] = -ViewVec.y / ViewVec.Length(); Step3.m[2][2] = Root / ViewVec.Length();
	//
	////We need an actualized Up vector
	//Vector4 Uppy = Vector4();
	//Uppy = Step3*Step2*UpVec;
	//
	////We spin the Up vector until it is aligned
	//Matrix4 Step4;
	//Step4.Identity();
	//Step4.m[0][0] = Uppy.y / Uppy.Length(); Step4.m[0][1] = -Uppy.x / Uppy.Length();
	//Step4.m[1][0] = Uppy.x / Uppy.Length(); Step4.m[1][1] = Uppy.y / Uppy.Length();
	//
	////and we have a general rotation matrix
	//GeneralRot =Step4*(Step3*(Step2*Step1));
}

Vector4 Camera::makePlane(Point4 P1, Point4 P2, Point4 P3) {
	Vector4 Res;

	Vector4 L1 = P2 - P1;
	Vector4 L2 = P3 - P1;

	Res = L1.Cross(L2);
	Res.w = -(Res.x*P1.x + Res.y*P1.y + Res.z*P1.z);

	return Res;
}
