/*!
******************************************************************************
\file    Army.h
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 3
\date    26-02-2020

\brief
.h for the Grid variables and functions

******************************************************************************/
#pragma once
#include "../Extras/Matrix4.h"
#include "Camera.h"
#include "..\cs250_rasterizer\Rasterizer.h"

//Individual block
struct Block {
public:
	Block();//constructor 

	//Scale, rotation, position, and respective matrices
	Point4 Scal;
	Point4 Pos;
	float Rot;
	Vector4 RotAx;

	Matrix4 Rota;
	Matrix4 Tras;
	Matrix4 Sca;

	//A concatenation to combine them all
	Matrix4 Conc;

	//Used to hold if every vertex is drawable or not
	std::vector<bool> CanI;
	std::vector<FACE> newF;
	std::vector<Point4> Colo;
	//Update, a multiplication by general rotation matrix and a function to draw the cube

	std::vector<Vector4> Normal();
	void Uppo();
	void Generat(Camera Cam);
	void Drew(bool mode);


	Camera camo;
	//All the vertices for the cube
	std::vector<Point4> vertex;
	std::vector<bool> faces;
	std::vector<bool> faceoff;
};

//All the grid
struct Legion
{
public:
	Legion();

	//All the vertices for the cube
	std::vector<Point4> vertices;
	//Each cube from the grid
	std::vector<Block> Soldier;


	//Update, a multiplication by general rotation matrix and a function to draw the cube
	void Update();
	void Generot(Camera Cam);
	void Draw(bool mode);

};