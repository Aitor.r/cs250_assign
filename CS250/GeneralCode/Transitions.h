/*!
******************************************************************************
\file    Transitions.h
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  A class that will have some of the Transformation matrices

******************************************************************************/
#pragma once
#include "../Extras/Matrix4.h"

class Transitions
{
public:
	Transitions();//Constructor

	Point4 Update(Point4 * Mat);

	Vector4 Top;
	Vector4 Bot;
	Vector4 Right;
	Vector4 Left;
	Vector4 Far;
	Vector4 Near;

	Matrix4 ViewPort;//Matrix to transform to viewport
	Matrix4 Proj;//Matrix to transform to Projection plane

	Matrix4 Comb;
	void Combine(Matrix4 cam);
};