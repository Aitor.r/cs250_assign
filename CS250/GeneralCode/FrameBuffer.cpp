/*!
******************************************************************************
\file    FrameBuffer.cpp
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  Definitions for the .h functions

******************************************************************************/
#include "FrameBuffer.h"

FrameBuffer * FrameBuffer::instance = NULL;

//Create a window and the thing to print
void FrameBuffer::Initialize() {
	//Create the window to put on screen
	window.create(sf::VideoMode(Thicc, Slicc), "SFML works!");

	//Where to store what to put in the window
	texture.create(Thicc, Slicc);
	image.create(Thicc, Slicc, sf::Color::White);
	
	//Initialize all to maximum value of z (1.0f)
	for (unsigned int i = 0; i < (Thicc*Slicc); i++)
		v[i] = 1;
}

//Set the pixel
void FrameBuffer::SetPixel(unsigned int x, unsigned int y, float z, sf::Color col) {
	if (x < Thicc && y < Slicc && x > 0 && y > 0) {//The pixel is in screen
		if (z < m[x][y]) {//It is closer than previously stored pixel
			m[x][y] = z;//Store new value
			image.setPixel(x, y, col);//Call the setpixel of the image
		}
	}
}