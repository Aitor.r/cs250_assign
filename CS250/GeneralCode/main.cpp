#define _CRT_SECURE_NO_WARNINGS
#define NOT 0.0f
#define YEP 0.1f

/*!
******************************************************************************
\file    main.cpp
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  Main. It's main

******************************************************************************/

#include "Classes.h"
#include "..\cs250_rasterizer\Rasterizer.h"
#include "..\cs250_parser\CS250Parser.h"
#include "FrameBuffer.h"
#include "Transitions.h"
#include "Army.h"
#include "Camera.h"

bool drawMode = true;//Bool variable for draw method
bool FPT = false;//First Person Tank
bool TPT = false;//Third Person Tank
bool FC = true;//Fixed camera
Camera General;

//IT'S MAIN. Things happen here.
int main()
{
	//Read file
	CS250Parser::LoadDataFromFile();

	General.Create();

	Legion Grid;//Creted Grid

	//Variables where to store the vertices. Maybe I could have them be saved in the Tank_ classes? Next time.
	static std::vector<Point4> vertices = CS250Parser::vertices;//8 edges

	std::vector<Vector4> MN;//Normals of the faces
	std::vector<bool> MB;//Bool for each possible faces
	std::vector<Vector4> TN;
	std::vector<bool> TB;
	std::vector<Vector4> RN;
	std::vector<bool> RB;
	std::vector<Vector4> LN;
	std::vector<bool> LB;

	std::vector<bool> CanI;	//to store a bool for every individual vertex
	std::vector<bool> CanT;	//to store a bool for every individual vertex
	std::vector<bool> CanW1;//to store a bool for every individual vertex
	std::vector<bool> CanW2;//to store a bool for every individual vertex

	//All to true
	for (unsigned int i = 0; i < vertices.size(); i++) {
		CanI.push_back(true);
		CanT.push_back(true);
		CanW1.push_back(true);
		CanW2.push_back(true);
	}

	//We set the grid vertices as well
	Grid.vertices = vertices;

	TankB MainB;//Create a Body for the tank
	MainB.vertices = vertices;

	//The tail of the plane
	Blocky Tail = Blocky(Point4(NOT, 10.0f, -15.0f), Point4(5.0f, 7.5f, 10.0f));
	Tail.Dad = &MainB;
	Tail.vertice = vertices;

	//Its wings
	Blocky LWing = Blocky(Point4(-17.5f, NOT, NOT), Point4(20.0f, 5.0f, 10.0f));
	LWing.Dad = &MainB;
	LWing.vertice = vertices;
	Blocky RWing = Blocky(Point4(17.5f, NOT, NOT), Point4(20.0f, 5.0f, 10.0f));
	RWing.Dad = &MainB;
	RWing.vertice = vertices;

	//Variables for the concatenation
	Matrix4 BodConc;
	Matrix4 MConc;

	//Variable to call the transformations
	Transitions FromTo;



	//While the window is open
	while (FB->window.isOpen())
	{
		//We create background
		FB->image.create(Thicc, Slicc, sf::Color::White);
		sf::Event event;

		//If we have to close
		while (FB->window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				FB->window.close();
		}

		//We close with space
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
			FB->window.close();
		}

		//Change the draw mode with 1 and 2 and camera with 3 4 and 5
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num1)){
			drawMode = true;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num2)) {
			drawMode = false;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num3)) {
			//Set to First person camera
			FPT = true;
			TPT = false;
			FC = false;
			General.CreateF(MainB);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num4)) {
			//Set to thrid person camera
			FPT = false;
			TPT = true;
			FC = false;
			General.Create(MainB);
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Num5)) {
			//Set to fixed camera
			FPT = false;
			TPT = false;
			FC = true;
			General.Create();
		}

		//Barrel roll(Reversed)
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A)) {
			MainB.Rotf += YEP;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D)) {
			MainB.Rotf -= YEP;
		}
		MainB.UpdateF();


		//Turn left/right(Unavailable)
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q)) {
			MainB.Rotu += YEP;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::E)) {
			MainB.Rotu -= YEP;
		}
		MainB.UpdateU();

		//Turn up/down(Unavailable)
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W)) {
			MainB.Rotr += YEP;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S)) {
			MainB.Rotr -= YEP;
		}
		MainB.UpdateR();

		//We move forward with Space
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {			
			//The position changes, maybe a little slow
			MainB.Pos += MainB.For;
		}

		//Approach/retreat in Third person with X and Z
		if (TPT && sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z) && General.distance< 200.0f) {
			General.distance += YEP*10;//Either use *10 or it would go at a snail's pace
		}
		if (TPT && sf::Keyboard::isKeyPressed(sf::Keyboard::Key::X) && General.distance>MainB.Scal.z) {
			General.distance -= YEP * 10;
		}

		//Move Up/Down in Third Person with H and Y
		if (TPT && sf::Keyboard::isKeyPressed(sf::Keyboard::Key::H) && General.height< 200.0f) {
			General.height += YEP * 10;
		}
		if (TPT && sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Y) && General.height>0.0f) {
			General.height -= YEP * 10;
		}


		//Update all 3 transformation matrices according to input
		Grid.Update();
		MainB.Update();
		Tail.Uppo();
		RWing.Uppo();
		LWing.Uppo();

		//Update camera depending on the mode
		if (TPT)
			General.Update(MainB);
		if (FPT)
			General.UpdateF(MainB);
		if (FC)
			General.Update();


		//Generate general rotation matrix
		General.GeneralGen();


		//Matrix concatenations. Could have put these somewhere else? Most likely. But that may end up taking me more time than what I would like to. Maybe for the next assignment
		BodConc = (MainB.Tras*MainB.Rota*MainB.Sca);
		MConc = MainB.Tras*MainB.Rota;


		//Concatenate all vertices and put them in Camera space
		for (unsigned int i = 0; i < CS250Parser::vertices.size(); i++) {
			MainB.vertices[i] = (BodConc*CS250Parser::vertices[i]);
			Tail.vertice[i] = (MConc*(Tail.Tras*Tail.Rota*Tail.Sca*CS250Parser::vertices[i]));
			RWing.vertice[i] = (MConc*(RWing.Tras*RWing.Rota*RWing.Sca*CS250Parser::vertices[i]));
			LWing.vertice[i] = (MConc*(LWing.Tras*LWing.Rota*LWing.Sca*CS250Parser::vertices[i]));

		}

		MN = MainB.Normal(MainB.vertices);
		TN = Tail.Normal(Tail.vertice);
		RN = RWing.Normal(RWing.vertice);
		LN = LWing.Normal(LWing.vertice);
		//Back face removal
		for (unsigned int i = 0; i < CS250Parser::faces.size(); i++) {
			float M = MN[i].Dot(Point4(MainB.vertices[CS250Parser::faces[i].indices[0]].x, MainB.vertices[CS250Parser::faces[i].indices[0]].y, MainB.vertices[CS250Parser::faces[i].indices[0]].z) - General.CoP);
			float T = TN[i].Dot(Point4(Tail.vertice[CS250Parser::faces[i].indices[0]].x, Tail.vertice[CS250Parser::faces[i].indices[0]].y, Tail.vertice[CS250Parser::faces[i].indices[0]].z) - General.CoP);
			float R = RN[i].Dot(Point4(RWing.vertice[CS250Parser::faces[i].indices[0]].x, RWing.vertice[CS250Parser::faces[i].indices[0]].y, RWing.vertice[CS250Parser::faces[i].indices[0]].z) - General.CoP);
			float L = LN[i].Dot(Point4(LWing.vertice[CS250Parser::faces[i].indices[0]].x, LWing.vertice[CS250Parser::faces[i].indices[0]].y, LWing.vertice[CS250Parser::faces[i].indices[0]].z) - General.CoP);

			if (M >= 0)
				MB.push_back(true);
			else
				MB.push_back(false);

			if (T >= 0)
				TB.push_back(true);
			else
				TB.push_back(false);

			if (R >= 0)
				RB.push_back(true);
			else
				RB.push_back(false);

			if (L >= 0)
				LB.push_back(true);
			else
				LB.push_back(false);
		}

		FromTo.Combine(General.GeneralRot);

		//Culling
		for (unsigned int i = 0; i < CS250Parser::vertices.size(); i++) {

			if (Checking(FromTo.Top, MainB.vertices[i]) || Checking(FromTo.Bot, MainB.vertices[i]) || Checking(FromTo.Left, MainB.vertices[i]) || Checking(FromTo.Right, MainB.vertices[i]))
				CanI[i] = false;
			if (Checking(FromTo.Top, Tail.vertice[i]) || Checking(FromTo.Bot, Tail.vertice[i]) || Checking(FromTo.Left, Tail.vertice[i]) || Checking(FromTo.Right, Tail.vertice[i]))
				CanT[i] = false;
			if (Checking(FromTo.Top, RWing.vertice[i]) || Checking(FromTo.Bot, RWing.vertice[i]) || Checking(FromTo.Left, RWing.vertice[i]) || Checking(FromTo.Right, RWing.vertice[i]))
				CanW1[i] = false;
			if (Checking(FromTo.Top, LWing.vertice[i]) || Checking(FromTo.Bot, LWing.vertice[i]) || Checking(FromTo.Left, LWing.vertice[i]) || Checking(FromTo.Right, LWing.vertice[i]))
				CanW2[i] = false;
		}
			MainB.Canwe = CanI;
			Tail.CanI = CanT;
			LWing.CanI = CanW1;
			LWing.CanI = CanW2;


			//HERE CLIPPING
			MainB.Clippo(MB, FromTo);
			Tail.Clippo(TB, FromTo);
			RWing.Clippo(RB, FromTo);
			LWing.Clippo(LB, FromTo);

		for (unsigned int i = 0; i < CS250Parser::vertices.size(); i++) {
			MainB.vertices[i] = General.GeneralRot*MainB.vertices[i];
			MainB.verticai[i] = General.GeneralRot*MainB.verticai[i];
			Tail.vertice[i] = General.GeneralRot*Tail.vertice[i];
			Tail.verticai[i] = General.GeneralRot*Tail.verticai[i];
			RWing.vertice[i] = General.GeneralRot*RWing.vertice[i];
			RWing.verticai[i] = General.GeneralRot*RWing.verticai[i];
			LWing.vertice[i] = General.GeneralRot*LWing.vertice[i];
			LWing.verticai[i] = General.GeneralRot*LWing.verticai[i];


			MainB.vertices[i] = FromTo.Update(&(MainB.vertices[i]));
			MainB.verticai[i] = FromTo.Update(&(MainB.verticai[i]));
			Tail.vertice[i] = FromTo.Update(&(Tail.vertice[i]));
			Tail.verticai[i] = FromTo.Update(&(Tail.verticai[i]));
			RWing.vertice[i] = FromTo.Update(&(RWing.vertice[i]));
			RWing.verticai[i] = FromTo.Update(&(RWing.verticai[i]));
			LWing.vertice[i] = FromTo.Update(&(LWing.vertice[i]));
			LWing.verticai[i] = FromTo.Update(&(LWing.verticai[i]));
		}


		//Transform grid to Camera space
		Grid.Generot(General);
		//Draw grid
		Grid.Draw(drawMode);

		//If we draw full triangles, Rasterize triangles
		if (drawMode) {
			RasterizeTriangle(MainB.verticai, MainB.Canwe, MainB.Colo, MainB.faceoff, MainB.newF);
			RasterizeTriangle(Tail.verticai, Tail.CanI, Tail.Colo, Tail.faceoff, Tail.newF);
			RasterizeTriangle(RWing.verticai, RWing.CanI, RWing.Colo, RWing.faceoff, RWing.newF);
			RasterizeTriangle(LWing.verticai, RWing.CanI, RWing.Colo, RWing.faceoff, RWing.newF);
		}
		else {//If we don't draw full triangles, Rasterize Lines
			RasterizeLine(MainB.verticai, MainB.Canwe, MainB.Colo, MainB.faceoff, MainB.newF);
			RasterizeLine(Tail.verticai, Tail.CanI, Tail.Colo, Tail.faceoff, Tail.newF);
			RasterizeLine(RWing.verticai, RWing.CanI, RWing.Colo, RWing.faceoff, RWing.newF);
			RasterizeLine(LWing.verticai, RWing.CanI, RWing.Colo, RWing.faceoff, RWing.newF);
		}

		
		MainB.verticai.clear();
		MainB.Canwe.clear();
		MainB.Colo.clear();
		MainB.faceoff.clear();
		MainB.newF.clear();

		TB.clear();
		Tail.verticai.clear();
		Tail.CanI.clear();
		Tail.Colo.clear();
		Tail.faceoff.clear();
		Tail.newF.clear();

		RB.clear();
		RWing.verticai.clear();
		RWing.CanI.clear();
		RWing.Colo.clear();
		RWing.faceoff.clear();
		RWing.newF.clear();
		LB.clear();
		LWing.verticai.clear();
		LWing.CanI.clear();
		LWing.Colo.clear();
		LWing.faceoff.clear();
		LWing.newF.clear();
		

		//Reset them to true
		for (unsigned int i = 0; i < CS250Parser::vertices.size(); i++) {
				CanI[i] = true;
				CanT[i] = true;
				CanW1[i] = true;
				CanW2[i] = true;
		}

		//Reset the buffer values
		for (unsigned int i = 0; i < Thicc*Slicc; i++)
			FB->v[i] = 1;

		//Update the image
		FB->window.clear();
		FB->texture.update(FB->image);
		FB->sprite.setTexture(FB->texture);
		FB->window.draw(FB->sprite);
		FB->window.display();
	}

	return 0;
}