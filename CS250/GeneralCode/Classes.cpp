/*!
******************************************************************************
\file    Classes.cpp
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  Definitions for the .h functions

******************************************************************************/
#include "Classes.h"
#include "../cs250_parser\CS250Parser.h"
#include "../cs250_rasterizer/Rasterizer.h"

//No magic numbers. Besides the default values. 
#define SIZE 3
#define INITPOS 0
#define Xpos 1
#define Ypos 2

//simple constructor
TankB::TankB() {
	Scal = Point4(15.0f, 12.5f, 40.0f, 1.0f);//Set variables to default values
	Pos = Point4(0.0f, 0.0f, -100.0f, 1.0f); //Set variables to default values
	Rot = 0.0f;								 //Set variables to default values
	Up = Vector4(0.0f, 1.0f, 0.0f, 0.0f); //Set variables to default values
	Right = Vector4(1.0f, 0.0f, 0.0f, 0.0f); //Set variables to default values
	For = Vector4(0.0f, 0.0f, 1.0f, 0.0f); //Set variables to default values

	RotUp.Identity(); RotF.Identity(); RotR.Identity();
	Rota.Identity(); Sca.Identity(); Tras.Identity();//Constructor, so set to identity the matrices
}

//Function that updates the matrices
void TankB::Update() {
	/*
	Matrix4 Posco; Posco.Identity();
	Posco.m[0][3] = -Pos.x; Posco.m[1][3] = -Pos.y; Posco.m[2][3] = -Pos.z;

	Matrix4 Tap = Matrix4(Right.x, Up.x, For.x, Pos.x,
				  		  Right.y, Up.y, For.y, Pos.y,
						  Right.z, Up.z, For.z, Pos.z,
						  0.0f, 0.0f, 0.0f, 1.0f);
						  */
	//Rotation matrix for the plane
	Rota = Matrix4(Right.x, Right.y, Right.z, 0.0f,
				   Up.x, Up.y, Up.z, 0.0f,
				   For.x, For.y, For.z, 0.0f,
				   0.0f, 0.0f, 0.0f, 1.0f);

	for (unsigned int i = INITPOS; i < SIZE; i++) {//Traslation and scale matrices
		Sca.m[i][i] = Scal.v[i];
		Tras.m[i][SIZE] = Pos.v[i];
	}
}
//Around forward
void TankB::UpdateF() {
	//What to rotate around
	Vector4 N = For;
	Matrix4 TAx; TAx.Identity();//Identity
	/*
	Matrix4 Posco; Posco.Identity();
	Posco.m[0][3] = -Pos.x; Posco.m[1][3] = -Pos.y; Posco.m[2][3] = -Pos.z;
	Matrix4 Tap = Matrix4(Right.x, Up.x, For.x, Pos.x,
		Right.y, Up.y, For.y, Pos.y,
		Right.z, Up.z, For.z, Pos.z,
		0.0f, 0.0f, 0.0f, 1.0f);

	Matrix4 T = Matrix4(Right.x, Right.y, Right.z, 0.0f,
		Up.x, Up.y, Up.z, 0.0f,
		For.x, For.y, For.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f)*Posco;
		*/
	//It is called Tensor product
	Matrix4 NxN = Matrix4(N.x*N.x, N.x*N.y, N.x*N.z, 0.0f, 
						  N.y*N.x, N.y*N.y, N.y*N.z, 0.0f, 
						  N.z*N.x, N.z*N.y, N.z*N.z, 0.0f,
						  0.0f, 0.0f, 0.0f, 0.0f);

	//Cross Product using Matrix multiplication
	Matrix4 � = Matrix4(); �.m[INITPOS][Xpos]=-N.z; �.m[INITPOS][Ypos]=N.y; �.m[Xpos][Ypos]=-N.x;
						   �.m[Xpos][INITPOS]=N.z; �.m[Ypos][INITPOS]=-N.y; �.m[Ypos][Xpos]=N.x;

	//Axis angle method
	TAx = TAx * cos(Rotf); TAx.m[3][3] = 1;
	RotF = TAx + (NxN)*(1 - cos(Rotf)) + � * sin(Rotf);
	
	Up = RotF * Up  + (Pos-RotF*Pos);
	Right = RotF * Right + (Pos - RotF * Pos);
	//Up = Tap * RotF*T*Up;
	//Right = Tap * RotF*T*Right;
	Rotf = 0.0f;
}
//Same but for Up vector
void TankB::UpdateU() {
	Vector4 N = Up;
	Matrix4 TAx; TAx.Identity();
	/*Matrix4 Posco; Posco.Identity();
	Posco.m[0][3] = -Pos.x; Posco.m[1][3] = -Pos.y; Posco.m[2][3] = -Pos.z;


	Matrix4 Tap = Matrix4(Right.x, Up.x, For.x, Pos.x,
		Right.y, Up.y, For.y, Pos.y,
		Right.z, Up.z, For.z, Pos.z,
		0.0f, 0.0f, 0.0f, 1.0f);

	Matrix4 T = Matrix4(Right.x, Right.y, Right.z, 0.0f,
		Up.x, Up.y, Up.z, 0.0f,
		For.x, For.y, For.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f)*Posco;
		*/
	Matrix4 NxN = Matrix4(N.x*N.x, N.x*N.y, N.x*N.z, 0.0f,
						  N.y*N.x, N.y*N.y, N.y*N.z, 0.0f,
						  N.z*N.x, N.z*N.y, N.z*N.z, 0.0f,
						  0.0f, 0.0f, 0.0f, 0.0f);

	Matrix4 � = Matrix4(); �.m[INITPOS][Xpos] = -N.z; �.m[INITPOS][Ypos] = N.y; �.m[Xpos][Ypos] = -N.x;
						   �.m[Xpos][INITPOS] = N.z; �.m[Ypos][INITPOS] = -N.y; �.m[Ypos][Xpos] = N.x;


	TAx = TAx * cos(Rotu); TAx.m[3][3] = 1;
	RotUp = TAx + (NxN)*(1 - cos(Rotu)) + � * sin(Rotu);

	For = RotUp * For + (Pos - RotUp * Pos);
	Right = RotUp * Right + (Pos - RotUp * Pos);
	//For = Tap * RotF*T*For;
	//Right = Tap * RotF*T*Right;
	Rotu = 0.0f;
}
//Same but for Right vector
void TankB::UpdateR() {
	Vector4 N = Right;
	Matrix4 TAx; TAx.Identity();
	/*Matrix4 Posco; Posco.Identity();
	Posco.m[0][3] = -Pos.x; Posco.m[1][3] = -Pos.y; Posco.m[2][3] = -Pos.z;


	Matrix4 Tap = Matrix4(Right.x, Up.x, For.x, Pos.x,
		Right.y, Up.y, For.y, Pos.y,
		Right.z, Up.z, For.z, Pos.z,
		0.0f, 0.0f, 0.0f, 1.0f);

	Matrix4 T = Matrix4(Right.x, Right.y, Right.z, 0.0f,
		Up.x, Up.y, Up.z, 0.0f,
		For.x, For.y, For.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f)*Posco;
		*/

	Matrix4 NxN = Matrix4(N.x*N.x, N.x*N.y, N.x*N.z, 0.0f,
						  N.y*N.x, N.y*N.y, N.y*N.z, 0.0f,
						  N.z*N.x, N.z*N.y, N.z*N.z, 0.0f,
						  0.0f, 0.0f, 0.0f, 0.0f);

	Matrix4 � = Matrix4(); �.m[INITPOS][Xpos] = -N.z; �.m[INITPOS][Ypos] = N.y; �.m[Xpos][Ypos] = -N.x;
						   �.m[Xpos][INITPOS] = N.z; �.m[Ypos][INITPOS] = -N.y; �.m[Ypos][Xpos] = N.x;

    TAx = TAx * cos(Rotr); TAx.m[3][3] = 1;
	RotR= TAx + (NxN)*(1 - cos(Rotr)) + � * sin(Rotr);

	For = RotR * For + (Pos - RotR * Pos);
	Up = RotR * Up + (Pos - RotR * Pos);
	//For = Tap * RotF*T*For;
	//Up = Tap * RotF*T*Up;
	Rotr = 0.0f;
}
//Constructor
Blocky::Blocky() {
	Scal = Point4(10.0f, 10.0f, 10.0f, 1.0f);//Set variables to default values
	Pos = Point4(0.0f, 0.0f, -100.0f, 1.0f); //Set variables to default values
	Rot = 0.0f;								 //Set variables to default values
	RotAx = Vector4(0.0f, 1.0f, 0.0f, 0.0f); //Set variables to default values

	Rota.Identity(); Sca.Identity(); Tras.Identity(); Conc.Identity();//Constructor, so set to identity the matrices
}

//Custom constructor
Blocky::Blocky(Point4 Posi, Point4 Scali) {
	Scal = Scali;//Set variables to default values
	Pos = Posi; //Set variables to default values
	Rot = 0.0f;								 //Set variables to default values
	RotAx = Vector4(0.0f, 1.0f, 0.0f, 0.0f); //Set variables to default values

	Rota.Identity(); Sca.Identity(); Tras.Identity();//Constructor, so set to identity the matrices
}

//An update function
void Blocky::Uppo() {
	Rota.m[INITPOS][INITPOS] = cos(Rot); Rota.m[INITPOS][Ypos] = sin(Rot);//Rotation matrix depends on the rotation 
	Rota.m[Ypos][INITPOS] = -sin(Rot); Rota.m[Ypos][Ypos] = cos(Rot);	  //Rotation matrix depends on the rotation 

	for (unsigned int i = INITPOS; i < SIZE; i++) {//Traslation and scale matrices
		Sca.m[i][i] = Scal.v[i];
		Tras.m[i][SIZE] = Pos.v[i];
	}

	//Concatenation
	Conc = Dad->Tras*Dad->Rota*(Tras*Rota*Sca);
	//Prepare the array of whether the vertices are good to go or not
	for (unsigned int i = 0; i<vertice.size(); i++)
		CanI.push_back(true);
}

//Function to transform/cull the blocks
void Blocky::Generat(Matrix4 Cam) {
	//Pass to camera space and perfomr the check for every vertex
	for (unsigned int i = 0; i < vertice.size(); i++) {
		vertice[i] = Cam*(Conc*vertice[i]);
		if (vertice[i].z > -CS250Parser::nearPlane)
			CanI[i] = false;
	}
}

void TankB::Clippo(const std::vector<bool> faces, Transitions FromTo) {
	verticai = vertices;

	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++)
	{

		if (faces[i]) {
			faceoff.push_back(true);
			FACE fac;
			fac.indices[0] = 0;
			fac.indices[1] = 0;
			fac.indices[2] = 0;
			newF.push_back(fac);
			Colo.push_back(Point4());
			continue;
		}

		if (((Canwe[CS250Parser::faces[i].indices[0]] == false) || (Canwe[CS250Parser::faces[i].indices[1]] == false)) || (Canwe[CS250Parser::faces[i].indices[2]] == false))
			continue;

		Point2D Ver1 = Point2D(vertices[CS250Parser::faces[i].indices[0]].x, vertices[CS250Parser::faces[i].indices[0]].y, vertices[CS250Parser::faces[i].indices[0]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);
		Point2D Ver2 = Point2D(vertices[CS250Parser::faces[i].indices[1]].x, vertices[CS250Parser::faces[i].indices[1]].y, vertices[CS250Parser::faces[i].indices[1]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);
		Point2D Ver3 = Point2D(vertices[CS250Parser::faces[i].indices[2]].x, vertices[CS250Parser::faces[i].indices[2]].y, vertices[CS250Parser::faces[i].indices[2]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);


		std::vector<Point2D> Res = Clip(FromTo.Near, Ver1, Ver2, Ver3);
		unsigned int maximum = Res.size();

		if (maximum == 3) {
			FACE fac;
			fac.indices[0] = CS250Parser::faces[i].indices[0];
			fac.indices[1] = CS250Parser::faces[i].indices[1];
			fac.indices[2] = CS250Parser::faces[i].indices[2];
			newF.push_back(fac);


			Colo.push_back(CS250Parser::colors[i]);
			faceoff.push_back(false);
		}
		else if (maximum == 4) {
			//Color is going to be the same, if we are clipping that means that it's going to be on one side
			Colo.push_back(CS250Parser::colors[i]);
			faceoff.push_back(false);

			FACE fac;
			for (unsigned int j = 0; j < 3; j++) {
				for (unsigned int k = 0; k < (Res.size() - 1); k++) {
					if (Res[k].x == Ver1.x && Res[k].y == Ver1.y && Res[k].z == Ver1.z)
						fac.indices[j] = CS250Parser::faces[i].indices[0];
					else if (Res[k].x == Ver2.x && Res[k].y == Ver2.y && Res[k].z == Ver2.z)
						fac.indices[j] = CS250Parser::faces[i].indices[1];
					else if (Res[k].x == Ver3.x && Res[k].y == Ver3.y && Res[k].z == Ver3.z)
						fac.indices[j] = CS250Parser::faces[i].indices[2];
					else {
						fac.indices[j] = verticai.size();
						verticai.push_back(Point4(Res[k].x, Res[k].y, Res[k].z));
						Canwe.push_back(true);
					}
				}
			}
			newF.push_back(fac);
			for (unsigned int j = 1; j < 3; j++) {
				for (unsigned int k = 1; k < Res.size(); k++) {
					if (Res[k].x == Ver1.x && Res[k].y == Ver1.y && Res[k].z == Ver1.z)
						fac.indices[j] = CS250Parser::faces[i].indices[0];
					else if (Res[k].x == Ver2.x && Res[k].y == Ver2.y && Res[k].z == Ver2.z)
						fac.indices[j] = CS250Parser::faces[i].indices[1];
					else if (Res[k].x == Ver3.x && Res[k].y == Ver3.y && Res[k].z == Ver3.z)
						fac.indices[j] = CS250Parser::faces[i].indices[2];
					else {
						fac.indices[j] = verticai.size();
						verticai.push_back(Point4(Res[k].x, Res[k].y, Res[k].z));
						Canwe.push_back(true);
					}
				}
			}
			faceoff.push_back(false);
			newF.push_back(fac);
		}
	}
}

std::vector<Vector4> TankB::Normal(const std::vector<Point4> vertices) {
	Vector4 Res;
	std::vector<Vector4> Rest;
	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++) {
		Point4 Ver1 = Point4(vertices[CS250Parser::faces[i].indices[0]].x, vertices[CS250Parser::faces[i].indices[0]].y, vertices[CS250Parser::faces[i].indices[0]].z);
		Point4 Ver2 = Point4(vertices[CS250Parser::faces[i].indices[1]].x, vertices[CS250Parser::faces[i].indices[1]].y, vertices[CS250Parser::faces[i].indices[1]].z);
		Point4 Ver3 = Point4(vertices[CS250Parser::faces[i].indices[2]].x, vertices[CS250Parser::faces[i].indices[2]].y, vertices[CS250Parser::faces[i].indices[2]].z);


		Vector4 L1 = Ver2 - Ver1;
		Vector4 L2 = Ver3 - Ver1;

		Res = L1.Cross(L2);

		Rest.push_back(Res);
	}
	return Rest;
}

void Blocky::Clippo(const std::vector<bool> faces, Transitions FromTo) {
	verticai = vertice;
	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++)
	{

		if (faces[i]) {
			faceoff.push_back(true);
			FACE fac;
			fac.indices[0] = 0;
			fac.indices[1] = 0;
			fac.indices[2] = 0;
			newF.push_back(fac);
			Colo.push_back(Point4());
			continue;
		}

		if (((CanI[CS250Parser::faces[i].indices[0]] == false) || (CanI[CS250Parser::faces[i].indices[1]] == false)) || (CanI[CS250Parser::faces[i].indices[2]] == false))
			continue;

		Point2D Ver1 = Point2D(vertice[CS250Parser::faces[i].indices[0]].x, vertice[CS250Parser::faces[i].indices[0]].y, vertice[CS250Parser::faces[i].indices[0]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);
		Point2D Ver2 = Point2D(vertice[CS250Parser::faces[i].indices[1]].x, vertice[CS250Parser::faces[i].indices[1]].y, vertice[CS250Parser::faces[i].indices[1]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);
		Point2D Ver3 = Point2D(vertice[CS250Parser::faces[i].indices[2]].x, vertice[CS250Parser::faces[i].indices[2]].y, vertice[CS250Parser::faces[i].indices[2]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);


		std::vector<Point2D> Res = Clip(FromTo.Near, Ver1, Ver2, Ver3);
		unsigned int maximum = Res.size();

		if (maximum == 3) {
			FACE fac;
			fac.indices[0] = CS250Parser::faces[i].indices[0];
			fac.indices[1] = CS250Parser::faces[i].indices[1];
			fac.indices[2] = CS250Parser::faces[i].indices[2];
			newF.push_back(fac);


			Colo.push_back(CS250Parser::colors[i]);
			faceoff.push_back(false);
		}
		else if (maximum == 4) {
			//Color is going to be the same, if we are clipping that means that it's going to be on one side
			Colo.push_back(CS250Parser::colors[i]);
			faceoff.push_back(false);

			FACE fac;
			for (unsigned int j = 0; j < 3; j++) {
				for (unsigned int k = 0; k < (Res.size() - 1); k++) {
					if (Res[k].x == Ver1.x && Res[k].y == Ver1.y && Res[k].z == Ver1.z)
						fac.indices[j] = CS250Parser::faces[i].indices[0];
					else if (Res[k].x == Ver2.x && Res[k].y == Ver2.y && Res[k].z == Ver2.z)
						fac.indices[j] = CS250Parser::faces[i].indices[1];
					else if (Res[k].x == Ver3.x && Res[k].y == Ver3.y && Res[k].z == Ver3.z)
						fac.indices[j] = CS250Parser::faces[i].indices[2];
					else {
						fac.indices[j] = verticai.size();
						verticai.push_back(Point4(Res[k].x, Res[k].y, Res[k].z));
						CanI.push_back(true);
					}
				}
			}
			newF.push_back(fac);
			for (unsigned int j = 1; j < 3; j++) {
				for (unsigned int k = 1; k < Res.size(); k++) {
					if (Res[k].x == Ver1.x && Res[k].y == Ver1.y && Res[k].z == Ver1.z)
						fac.indices[j] = CS250Parser::faces[i].indices[0];
					else if (Res[k].x == Ver2.x && Res[k].y == Ver2.y && Res[k].z == Ver2.z)
						fac.indices[j] = CS250Parser::faces[i].indices[1];
					else if (Res[k].x == Ver3.x && Res[k].y == Ver3.y && Res[k].z == Ver3.z)
						fac.indices[j] = CS250Parser::faces[i].indices[2];
					else {
						fac.indices[j] = verticai.size();
						verticai.push_back(Point4(Res[k].x, Res[k].y, Res[k].z));
						CanI.push_back(true);
					}
				}
			}
			faceoff.push_back(false);
			newF.push_back(fac);
		}
	}
}

std::vector<Vector4> Blocky::Normal(const std::vector<Point4> vertices) {
	Vector4 Res;
	std::vector<Vector4> Rest;
	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++) {
		Point4 Ver1 = Point4(vertices[CS250Parser::faces[i].indices[0]].x, vertices[CS250Parser::faces[i].indices[0]].y, vertices[CS250Parser::faces[i].indices[0]].z);
		Point4 Ver2 = Point4(vertices[CS250Parser::faces[i].indices[1]].x, vertices[CS250Parser::faces[i].indices[1]].y, vertices[CS250Parser::faces[i].indices[1]].z);
		Point4 Ver3 = Point4(vertices[CS250Parser::faces[i].indices[2]].x, vertices[CS250Parser::faces[i].indices[2]].y, vertices[CS250Parser::faces[i].indices[2]].z);


		Vector4 L1 = Ver2 - Ver1;
		Vector4 L2 = Ver3 - Ver1;

		Res = L1.Cross(L2);

		Rest.push_back(Res);
	}
	return Rest;
}