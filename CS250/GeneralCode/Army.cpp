/*!
******************************************************************************
\file    Army.cpp
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 3
\date    26-02-2020

\brief
Function definitions for the grid

******************************************************************************/
#include "Army.h"
#include "../cs250_parser\CS250Parser.h"
#include "Transitions.h"

#define SIZE 3		 //variable definitions
#define INITPOS 0	 //variable definitions
#define Xpos 1		 //variable definitions
#define Ypos 2		 //variable definitions
#define PlaOff 50.0f //variable definitions
#define PlaIx -100.0f//variable definitions

//constructor
Block::Block() {
	Scal = Point4(10.0f, 10.0f, 10.0f, 1.0f);//Set variables to default values
	Pos = Point4(0.0f, 0.0f, -100.0f, 1.0f); //Set variables to default values
	Rot = 0.0f;								 //Set variables to default values
	RotAx = Vector4(0.0f, 1.0f, 0.0f, 0.0f); //Set variables to default values

	Rota.Identity(); Sca.Identity(); Tras.Identity(); Conc.Identity();//Constructor, so set to identity the matrices
	
	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++)
		faces.push_back(true);
}

//An update function
void Block::Uppo() {
	Rota.m[INITPOS][INITPOS] = cos(Rot); Rota.m[INITPOS][Ypos] = sin(Rot);//Rotation matrix depends on the rotation 
	Rota.m[Ypos][INITPOS] = -sin(Rot); Rota.m[Ypos][Ypos] = cos(Rot);	  //Rotation matrix depends on the rotation 

	for (unsigned int i = INITPOS; i < SIZE; i++) {//Traslation and scale matrices
		Sca.m[i][i] = Scal.v[i];
		Tras.m[i][SIZE] = Pos.v[i];
	}

	//Concatenation
	Conc = Tras*Rota*Sca;
	//Prepare the array of whether the vertices are good to go or not
	for(unsigned int i=0; i<vertex.size(); i++)
		CanI.push_back(true);
}

//Stores the camara and does back-face, cull y clipping
void Block::Generat(Camera Cam) {
	//Pass to camera space and perfomr the check for every vertex
	std::vector<Vector4> Norms;
	Transitions FromTo;
	FromTo.Combine(Cam.GeneralRot);


	for (unsigned int i = 0; i < vertex.size(); i++) {
		vertex[i] = (Conc*vertex[i]);
	}
	Norms = Normal();
	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++) {
		float check = Norms[i].Dot(Point4(vertex[CS250Parser::faces[i].indices[0]].x, vertex[CS250Parser::faces[i].indices[0]].y, vertex[CS250Parser::faces[i].indices[0]].z) - Cam.CoP);
		if (check <= 0)
			faces[i] = true;
		else
			faces[i] = false;

	}
	for (unsigned int i = 0; i < vertex.size(); i++) {
		if ((Checking(FromTo.Top, vertex[i])==false || (Checking(FromTo.Bot, vertex[i]))==false) || (Checking(FromTo.Left, vertex[i]) == false || Checking(FromTo.Right, vertex[i]) == false))
			CanI[i] = false;
	}

	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++)
	{
		
		if (faces[i]) {
			faceoff.push_back(true);
			FACE fac;
			fac.indices[0] = 0;
			fac.indices[1] = 0;
			fac.indices[2] = 0;
			newF.push_back(fac);
			Colo.push_back(Point4());
			continue;
		}

		if (((CanI[CS250Parser::faces[i].indices[0]] == false) || (CanI[CS250Parser::faces[i].indices[1]] == false)) || (CanI[CS250Parser::faces[i].indices[2]] == false))
			continue;

		Point2D Ver1 = Point2D(vertex[CS250Parser::faces[i].indices[0]].x, vertex[CS250Parser::faces[i].indices[0]].y, vertex[CS250Parser::faces[i].indices[0]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);
		Point2D Ver2 = Point2D(vertex[CS250Parser::faces[i].indices[1]].x, vertex[CS250Parser::faces[i].indices[1]].y, vertex[CS250Parser::faces[i].indices[1]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);
		Point2D Ver3 = Point2D(vertex[CS250Parser::faces[i].indices[2]].x, vertex[CS250Parser::faces[i].indices[2]].y, vertex[CS250Parser::faces[i].indices[2]].z, CS250Parser::colors[i].x, CS250Parser::colors[i].y, CS250Parser::colors[i].z);


		std::vector<Point2D> Res = Clip(FromTo.Near, Ver1, Ver2, Ver3);
		unsigned int maximum = Res.size();

		if (maximum == 3) {
			FACE fac;
			fac.indices[0] = CS250Parser::faces[i].indices[0];
			fac.indices[1] = CS250Parser::faces[i].indices[1];
			fac.indices[2] = CS250Parser::faces[i].indices[2];
			newF.push_back(fac);


			Colo.push_back(CS250Parser::colors[i]);
			faceoff.push_back(false);
		}
		else if(maximum ==4){
			//Color is going to be the same, if we are clipping that means that it's going to be on one side
			Colo.push_back(CS250Parser::colors[i]);
			faceoff.push_back(false);

			FACE fac;
			for (unsigned int j = 0; j < 3; j++) {
				for (unsigned int k = 0; k < (Res.size()-1); k++) {
					if (Res[k].x == Ver1.x && Res[k].y == Ver1.y && Res[k].z == Ver1.z)
						fac.indices[j] = CS250Parser::faces[i].indices[0];
					else if (Res[k].x == Ver2.x && Res[k].y == Ver2.y && Res[k].z == Ver2.z)
						fac.indices[j] = CS250Parser::faces[i].indices[1];
					else if (Res[k].x == Ver3.x && Res[k].y == Ver3.y && Res[k].z == Ver3.z)
						fac.indices[j] = CS250Parser::faces[i].indices[2];
					else {
						fac.indices[j] = vertex.size();
						vertex.push_back(Point4(Res[k].x, Res[k].y, Res[k].z));
						CanI.push_back(true);
					}
				}
			}
			newF.push_back(fac);
			for (unsigned int j = 1; j < 3; j++) {
				for (unsigned int k = 1; k < Res.size(); k++) {
					if (Res[k].x == Ver1.x && Res[k].y == Ver1.y && Res[k].z == Ver1.z)
						fac.indices[j] = CS250Parser::faces[i].indices[0];
					else if (Res[k].x == Ver2.x && Res[k].y == Ver2.y && Res[k].z == Ver2.z)
						fac.indices[j] = CS250Parser::faces[i].indices[1];
					else if (Res[k].x == Ver3.x && Res[k].y == Ver3.y && Res[k].z == Ver3.z)
						fac.indices[j] = CS250Parser::faces[i].indices[2];
					else {
						fac.indices[j] = vertex.size();
						vertex.push_back(Point4(Res[k].x, Res[k].y, Res[k].z));
						CanI.push_back(true);
					}
				}
			}
			faceoff.push_back(false);
			newF.push_back(fac);

		}


	}

	camo = Cam;
}

//A function to draw
void Block::Drew(bool mode) {

	Transitions FromTo;//This for tranfosrmations

	//Update the vertices to NDC space
	for (unsigned int i = 0; i < vertex.size(); i++) {
		vertex[i] = camo.GeneralRot *vertex[i];
		vertex[i] = FromTo.Update(&(vertex[i]));
	}
	//Draw if we can
	if (mode)
		RasterizeTriangle(vertex, CanI, Colo, faceoff, newF);
	else
		RasterizeLine(vertex, CanI, Colo, faceoff, newF);
	
	while(!newF.empty())
		newF.clear();
	while(!faceoff.empty())
		faceoff.clear();
	while(!Colo.empty())
		Colo.clear();

	//Reset CanI
	while (!CanI.empty()) 
		CanI.clear();

}

std::vector<Vector4> Block::Normal() {
	Vector4 Res;
	std::vector<Vector4> Rest;
	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++) {
		Point4 Ver1 = Point4(vertex[CS250Parser::faces[i].indices[0]].x, vertex[CS250Parser::faces[i].indices[0]].y, vertex[CS250Parser::faces[i].indices[0]].z);
		Point4 Ver2 = Point4(vertex[CS250Parser::faces[i].indices[1]].x, vertex[CS250Parser::faces[i].indices[1]].y, vertex[CS250Parser::faces[i].indices[1]].z);
		Point4 Ver3 = Point4(vertex[CS250Parser::faces[i].indices[2]].x, vertex[CS250Parser::faces[i].indices[2]].y, vertex[CS250Parser::faces[i].indices[2]].z);


		Vector4 L1 = Ver2 - Ver1;
		Vector4 L2 = Ver3 - Ver1;

		Res = L1.Cross(L2);

		Rest.push_back(Res);
	}
	return Rest;
}

//Grid constructor
Legion::Legion() {
	//The initial Position for the first block
	Point4 Positions = Point4(PlaIx, 0.0f, PlaIx - PlaOff, 1.0f);
	//25 times, 5 per row
	for (unsigned int i = 0; i < 5; i++) {
		//We reset the x at the beginning of each row, and move a little bit in the z axis
		Positions.x = PlaIx; Positions.z += PlaOff;
		for (unsigned int k = 0; k < 5; k++) {
			Soldier.push_back(Block());//Create a block
			Soldier.back().Pos = Positions;//At this position
			Positions.x += PlaOff;//next position
		}
	}
}

//Grid update
void Legion::Update() {
	//We update all the Blocks
	for (unsigned int i = 0; i < Soldier.size(); i++) {
		Soldier[i].vertex = vertices;
		Soldier[i].Uppo();
	}
}

//Grid general rotation
void Legion::Generot(Camera Cam) {
	//Move all the blocks to camera space
	for (unsigned int i = 0; i < Soldier.size(); i++) {
		Soldier[i].Generat(Cam);
	}
}

//Draw the blocks
void Legion::Draw(bool mode) {
	for (unsigned int i = 0; i < Soldier.size(); i++)
		Soldier[i].Drew(mode);
}