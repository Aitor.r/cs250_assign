/*!
*	\file		MathUtilities.h
*	\brief		Useful definitions for the Math Assignment.
*	\details	Not zero and a couple of definitions.
*	\author		Aitor Rodriguez - aitor.r@digipen.edu
*	\date		13/01/2020
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#ifndef MATHUTILITIES_H
#define MATHUTILITIES_H

#include "stdlib.h"     // rand

// Do not change
static const float EPSILON = 0.0001f;

// Macro definitions
#define isZero(x) ((x < EPSILON) && (x > -EPSILON))
#define isEqual(x,y) (((x >= y) ? (x-y) : (y-x)) < EPSILON)

#endif