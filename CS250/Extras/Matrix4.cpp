/*!
*	\file		Matrix4.cpp
*	\brief		Function definitions for the Math Assignment.
*	\details	Performs the calculations needed for Matrices.
*	\author		Aitor Rodriguez - aitor.r@digipen.edu
*	\date		13/01/2020
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#include "Matrix4.h"
#include "MathUtilities.h"
#define MaxS 16
#define row 4

/*!
*	\brief	Default constructs a matrix of 16 parameters
*   \return no return
*/
Matrix4::Matrix4(void){
	//For loop to set all to 0
	for(unsigned int i = 0; i<MaxS; i++){
		v[i] = 0;
	}
}

/*!
*	\brief	Constructs a new matrix based on input
*	\param	rhs		matrix to use as base
*   \return no return
*/
Matrix4::Matrix4(const Matrix4& rhs){
	//For loop to set all to the input
	for(unsigned int i = 0; i<MaxS; i++){
		v[i] = rhs.v[i];
	}
}

/*!
*	\brief	Constructs a new point based on input
*	\param	mm00	    value of 0,0 position
*	\param	mm01	    value of 0,1 position
*	\param	mm02	    value of 0,2 position
*	\param	mm03	    value of 0,3 position
*	\param	mm10	    value of 1,0 position
*	\param	mm11	    value of 1,1 position
*	\param	mm12	    value of 1,2 position
*	\param	mm13	    value of 1,3 position
*	\param	mm20	    value of 2,0 position
*	\param	mm21	    value of 2,1 position
*	\param	mm22	    value of 2,2 position
*	\param	mm23	    value of 2,3 position
*	\param	mm30	    value of 3,0 position
*	\param	mm31	    value of 3,1 position
*	\param	mm32	    value of 3,2 position
*	\param	mm33	    value of 3,3 position
*   \return no return
*/
Matrix4::Matrix4(float mm00, float mm01, float mm02, float mm03,
        float mm10, float mm11, float mm12, float mm13,
        float mm20, float mm21, float mm22, float mm23,
        float mm30, float mm31, float mm32, float mm33)
{
    //Set each value where it belongs
	v[0] = mm00; v[1] = mm01; v[2] = mm02; v[3] = mm03;
	v[4] = mm10; v[5] = mm11; v[6] = mm12; v[7] = mm13;
	v[8] = mm20; v[9] = mm21; v[10] = mm22; v[11] = mm23;
	v[12] = mm30; v[13] = mm31; v[14] = mm32; v[15] = mm33;
}

/*!
*	\brief	Changes our matrix based on input
*	\param	rhs		matrix to use as base
*   \return Our changed matrix (this)
*/
Matrix4& Matrix4::operator=(const Matrix4& rhs){
	//Loop to set each position correctly
	for(unsigned int i = 0; i<MaxS; i++){
		v[i] = rhs.v[i];
	}

	return *this;
}

/*!
*	\brief	multiplication between matrix and vector
*	\param	rhs		Vector to use
*   \return Result vector
*/
Vector4 Matrix4::operator*(const Vector4& rhs) const{
	Vector4 Res;//Result variable

	//MATH 
	Res.x = (v[0] * rhs.x) + (v[1] * rhs.y)+(v[2] * rhs.z) +(v[3]*rhs.w);
	Res.y = (v[4] * rhs.x) + (v[5] * rhs.y)+(v[6] * rhs.z) +(v[7]*rhs.w);
	Res.z = (v[8] * rhs.x) + (v[9] * rhs.y)+(v[10] * rhs.z)+(v[11]*rhs.w);
	Res.w = (v[12] * rhs.x) + (v[13] * rhs.y)+(v[14] * rhs.z)+(v[15]*rhs.w);

	return Res;
}

/*!
*	\brief	multiplication between matrix and point
*	\param	rhs		point to use
*   \return Result point
*/
Point4 Matrix4::operator*(const Point4& rhs) const{
	Point4 Res;//Result variable

	//MATH 
	Res.x = (v[0] * rhs.x) + (v[1] * rhs.y)+(v[2] * rhs.z)+(v[3]*rhs.w);
	Res.y = (v[4] * rhs.x) + (v[5] * rhs.y)+(v[6] * rhs.z)+(v[7]*rhs.w);
	Res.z = (v[8] * rhs.x) + (v[9] * rhs.y)+(v[10] * rhs.z)+(v[11]*rhs.w);
	Res.w = (v[12] * rhs.x) + (v[13] * rhs.y)+(v[14] * rhs.z)+(v[15]*rhs.w);

	return Res;
}

/*!
*	\brief	Addition between Matrices
*	\param	rhs		Matrix to use
*   \return Result Matrix
*/
Matrix4 Matrix4::operator+(const Matrix4& rhs) const{
	Matrix4 Res;//Result variable

	//MATH 
	for(unsigned int i = 0; i<MaxS; i++){
		Res.v[i] = v[i] + rhs.v[i];
	}

	return Res;
}

/*!
*	\brief	Substraction between Matrices
*	\param	rhs		Matrix to use
*   \return Result Matrix
*/
Matrix4 Matrix4::operator-(const Matrix4& rhs) const{
	Matrix4 Res;//Result variable

	//MATH 
	for(unsigned int i = 0; i<MaxS; i++){
		Res.v[i] = v[i] - rhs.v[i];
	}
	
	return Res;
}

/*!
*	\brief	Multiplication between Matrices
*	\param	rhs		Matrix to use
*   \return Result Matrix
*/
Matrix4 Matrix4::operator*(const Matrix4& rhs) const{
	Matrix4 Res;//Result variable

	//MATH 
	Res.m[0][0] = (m[0][0]*rhs.m[0][0])+(m[0][1]*rhs.m[1][0])+(m[0][2]*rhs.m[2][0])+(m[0][3]*rhs.m[3][0]);
	Res.m[0][1] = (m[0][0]*rhs.m[0][1])+(m[0][1]*rhs.m[1][1])+(m[0][2]*rhs.m[2][1])+(m[0][3]*rhs.m[3][1]);
	Res.m[0][2] = (m[0][0]*rhs.m[0][2])+(m[0][1]*rhs.m[1][2])+(m[0][2]*rhs.m[2][2])+(m[0][3]*rhs.m[3][2]);
	Res.m[0][3] = (m[0][0]*rhs.m[0][3])+(m[0][1]*rhs.m[1][3])+(m[0][2]*rhs.m[2][3])+(m[0][3]*rhs.m[3][3]);
	
	Res.m[1][0] = (m[1][0]*rhs.m[0][0])+(m[1][1]*rhs.m[1][0])+(m[1][2]*rhs.m[2][0])+(m[1][3]*rhs.m[3][0]);
	Res.m[1][1] = (m[1][0]*rhs.m[0][1])+(m[1][1]*rhs.m[1][1])+(m[1][2]*rhs.m[2][1])+(m[1][3]*rhs.m[3][1]);
	Res.m[1][2] = (m[1][0]*rhs.m[0][2])+(m[1][1]*rhs.m[1][2])+(m[1][2]*rhs.m[2][2])+(m[1][3]*rhs.m[3][2]);
	Res.m[1][3] = (m[1][0]*rhs.m[0][3])+(m[1][1]*rhs.m[1][3])+(m[1][2]*rhs.m[2][3])+(m[1][3]*rhs.m[3][3]);
	
	Res.m[2][0] = (m[2][0]*rhs.m[0][0])+(m[2][1]*rhs.m[1][0])+(m[2][2]*rhs.m[2][0])+(m[2][3]*rhs.m[3][0]);
	Res.m[2][1] = (m[2][0]*rhs.m[0][1])+(m[2][1]*rhs.m[1][1])+(m[2][2]*rhs.m[2][1])+(m[2][3]*rhs.m[3][1]);
	Res.m[2][2] = (m[2][0]*rhs.m[0][2])+(m[2][1]*rhs.m[1][2])+(m[2][2]*rhs.m[2][2])+(m[2][3]*rhs.m[3][2]);
	Res.m[2][3] = (m[2][0]*rhs.m[0][3])+(m[2][1]*rhs.m[1][3])+(m[2][2]*rhs.m[2][3])+(m[2][3]*rhs.m[3][3]);
	
	Res.m[3][0] = (m[3][0]*rhs.m[0][0])+(m[3][1]*rhs.m[1][0])+(m[3][2]*rhs.m[2][0])+(m[3][3]*rhs.m[3][0]);
	Res.m[3][1] = (m[3][0]*rhs.m[0][1])+(m[3][1]*rhs.m[1][1])+(m[3][2]*rhs.m[2][1])+(m[3][3]*rhs.m[3][1]);
	Res.m[3][2] = (m[3][0]*rhs.m[0][2])+(m[3][1]*rhs.m[1][2])+(m[3][2]*rhs.m[2][2])+(m[3][3]*rhs.m[3][2]);
	Res.m[3][3] = (m[3][0]*rhs.m[0][3])+(m[3][1]*rhs.m[1][3])+(m[3][2]*rhs.m[2][3])+(m[3][3]*rhs.m[3][3]);

	return Res;
}

/*!
*	\brief	addition between matrices changing ours
*	\param	rhs		matrix to use
*   \return Our altered matrix
*/
Matrix4& Matrix4::operator+=(const Matrix4& rhs){
	Matrix4 Use = *this;//Create variable with our matrix
	*this = (Use+rhs);  //Overwrite our matrix with math
	return *this;
}
/*!
*	\brief	Substraction between matrices changing ours
*	\param	rhs		matrix to use
*   \return Our altered matrix
*/
Matrix4& Matrix4::operator-=(const Matrix4& rhs){
	Matrix4 Use = *this;//Create variable with our matrix
	*this = (Use-rhs);  //Overwrite our matrix with math
	return *this;
}
/*!
*	\brief	Multiplication between matrices changing ours
*	\param	rhs		matrix to use
*   \return Our altered matrix
*/
Matrix4& Matrix4::operator*=(const Matrix4& rhs){
	Matrix4 Use = *this;//Create variable with our matrix
	*this = (Use*rhs);  //Overwrite our matrix with math
	return *this;
}

/*!
*	\brief	Multiplication between matrix and float
*	\param	rhs		float to use
*   \return result matrix
*/
Matrix4 Matrix4::operator*(const float rhs) const{
	Matrix4 Res = *this;//Result variable

	//MATH
	for(unsigned int i = 0; i<MaxS; i++){
		Res.v[i] = rhs*Res.v[i];
	}
	return Res;
}
/*!
*	\brief	Division between matrix and float
*	\param	rhs		float to use
*   \return result matrix
*/
Matrix4 Matrix4::operator/(const float rhs) const{
	Matrix4 Res = *this;//Result variable

	//MATH
	for(unsigned int i = 0; i<MaxS; i++){
		Res.v[i] = Res.v[i]/rhs;
	}
	return Res;
}

/*!
*	\brief	Multiplication between matrix and float changing ours
*	\param	rhs		float to use
*   \return Our altered matrix
*/
Matrix4& Matrix4::operator*=(const float rhs){
	Matrix4 Use = *this;//Create variable with our matrix
	*this = (Use*rhs);	//Overwrite our matrix with math
	return *this;
}
/*!
*	\brief	Division between matrix and float changing ours
*	\param	rhs		float to use
*   \return Our altered matrix
*/
Matrix4& Matrix4::operator/=(const float rhs){
	Matrix4 Use = *this;//Create variable with our matrix
	*this = (Use/rhs);	//Overwrite our matrix with math
	return *this;
}

/*!
*	\brief	comparison between 2 matrices
*	\param	rhs		point to use
*   \return true or false
*/
bool Matrix4::operator==(const Matrix4& rhs) const{
	//For loop to check every parameter of the matrix
	for(unsigned int i = 0; i<row; i++){
		for(unsigned int k = 0; k<row; k++){
			if(!isEqual(m[i][k],rhs.m[i][k]))//If they aren't equal, it`s false
				return false;
		}
	}
	return true;//Everything checks out
}
/*!
*	\brief	inverse comparison between 2 matrices
*	\param	rhs		point to use
*   \return true or false
*/
bool Matrix4::operator!=(const Matrix4& rhs) const{
	return !(*this==rhs);//Everything checks out
}

/*!
*	\brief	Resets our matrix to the 0 values
*   \return none
*/
void Matrix4::Zero(void){
	//Loop to set everything to zero
	for(unsigned int i = 0; i<MaxS; i++){
		v[i] = 0;
	}
}

void Matrix4::Identity(void){
	//Loop to go through the entire matrix
	for(unsigned int i = 0; i<row; i++){
		for(unsigned int k = 0; k<row; k++){
			if(i==k)//If we are in the axis, 1
				m[i][k] = 1;
			else//else 0
				m[i][k] = 0;
		}
	}
}