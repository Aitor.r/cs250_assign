/*!
*	\file		Vector4.cpp
*	\brief		Function definitions for the Math Assignment.
*	\details	Performs the calculations needed for Matrices.
*	\author		Aitor Rodriguez - aitor.r@digipen.edu
*	\date		13/01/2020
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#include "Vector4.h"
#include "MathUtilities.h"
#include <math.h>
#define Max 4

/*!
*	\brief	Default constructs a vector of 4 parameters
*   \return no return
*/
Vector4::Vector4(void){
	//for loop to set all to 0
	for(unsigned int i=0; i<Max; i++){
		v[i]=0;
	}
}

/*!
*	\brief	Constructs a new vector based on input
*	\param	rhs		Vector to use as base
*   \return no return
*/
Vector4::Vector4(const Vector4& rhs){
	//for loop to set all to the input values
	for(unsigned int i=0; i<Max; i++){
		v[i]=rhs.v[i];
	}
}

/*!
*	\brief	Constructs a new vector based on input
*	\param	xx	    x value of vector
*	\param	yy	    y value of vector
*	\param	zz	    z value of vector
*	\param	ww	    w value of vector
*   \return no return
*/
Vector4::Vector4(float xx, float yy, float zz, float ww){
	//Sets values one by one by hand
	x=xx;  y=yy;  z=zz;
	w=ww;
}

/*!
*	\brief	Changes our vector based on input
*	\param	rhs		Vector to use as base
*   \return Our changed vector (this)
*/
Vector4& Vector4::operator=(const Vector4& rhs){
	//For loop to change values
	for(unsigned int i = 0; i<Max; i++){
		v[i] = rhs.v[i];
	}

	return *this;
}

/*!
*	\brief	Unary negation to the vector
*   \return The unary negated vector
*/
Vector4 Vector4::operator-(void) const{
	Vector4 Res;//Variable to return
	
	//For loop to change values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = -v[i];
	}

	return Res;
}

/*!
*	\brief	Addition between vectors
*	\param	rhs		Vector to use
*   \return Result vector
*/
Vector4 Vector4::operator+(const Vector4& rhs) const{
	Vector4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = rhs.v[i]+v[i];
	}

	return Res;
}
/*!
*	\brief	Substraction between vectors
*	\param	rhs		Vector to use
*   \return Result vector
*/
Vector4 Vector4::operator-(const Vector4& rhs) const{
	Vector4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = v[i]-rhs.v[i];
	}

	return Res;
}

/*!
*	\brief	multiplication between vector amd float
*	\param	rhs		Float to use
*   \return Result vector
*/
Vector4 Vector4::operator*(const float rhs) const{
	Vector4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = v[i]*rhs;
	}

	return Res;
}
/*!
*	\brief	division between vectors
*	\param	rhs		Vector to use
*   \return Result vector
*/
Vector4 Vector4::operator/(const float rhs) const{
	Vector4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = v[i]/rhs;
	}

	return Res;
}


/*!
*	\brief	addition between vectors changing ours
*	\param	rhs		Vector to use
*   \return Our altered vector
*/
Vector4& Vector4::operator+=(const Vector4& rhs){
	Vector4 Use = *this;//Create variable with our vector
	*this = (Use + rhs);//Overwrite our vector with math

	return *this;
}
/*!
*	\brief	substraction between vectors changing ours
*	\param	rhs		Vector to use
*   \return Our altered vector
*/
Vector4& Vector4::operator-=(const Vector4& rhs){
	Vector4 Use = *this;//Create variable with our vector
	*this = (Use - rhs);//Overwrite our vector with math

	return *this;
}
/*!
*	\brief	multiplication between vectors changing ours
*	\param	rhs		Vector to use
*   \return Our altered vector
*/
Vector4& Vector4::operator*=(const float rhs){
	Vector4 Use = *this;//Create variable with our vector
	*this = (Use * rhs);//Overwrite our vector with math

	return *this;
}
/*!
*	\brief	division between vectors changing ours
*	\param	rhs		Vector to use
*   \return Our altered vector
*/
Vector4& Vector4::operator/=(const float rhs){
	Vector4 Use = *this;//Create variable with our vector
	*this = (Use / rhs);//Overwrite our vector with math

	return *this;
}

/*!
*	\brief	comparation between 2 vectors
*	\param	rhs		Vector to use
*   \return true or false
*/
bool Vector4::operator==(const Vector4& rhs) const{
	for(unsigned int i=0; i<Max; i++){//for to check all the parameters
		if(!isEqual(v[i], rhs.v[i]))//If it isn't equal, return false
			return false;
	}
	return true;//All went well
}
/*!
*	\brief	inversed comparation between 2 vectors
*	\param	rhs		Vector to use
*   \return true or false
*/
bool Vector4::operator!=(const Vector4& rhs) const{

	return !(*this == rhs);//All went well
}

/*!
*	\brief	dot product between 2 vectors
*	\param	rhs		Vector to use
*   \return resulting float
*/
float Vector4::Dot(const Vector4& rhs) const{
	return (x*rhs.x + y*rhs.y + z*rhs.z);//Math
}

/*!
*	\brief	Cross product between 2 vectors
*	\param	rhs		Vector to use
*   \return resulting Vector
*/
Vector4 Vector4::Cross(const Vector4& rhs) const{
	Vector4 Res;//REsult value
	
	//MATH
	Res.x = (y*rhs.z - z*rhs.y);
	Res.y = -(x*rhs.z - z*rhs.x);
	Res.z = (x*rhs.y - y*rhs.x);

	return Res;
}

/*!
*	\brief	Length of our vectors
*   \return resulting float
*/
float Vector4::Length(void) const{
	return sqrtf((x*x)+(y*y)+(z*z)+(w*w));//So no warnings
}
/*!
*	\brief	Squared Length of our vectors
*   \return resulting float
*/
float Vector4::LengthSq(void) const{
	return ((x*x)+(y*y)+(z*z)+(w*w));
}

/*!
*	\brief	Our vector is normalized if the Length is not 0
*   \return none
*/
void Vector4::Normalize(void){
	float lengthy = Length();
	if(isZero(lengthy))//Check if the length will be 0
		return;

	//It wasn't, so MATH
	for(unsigned int i =0; i<Max; i++){
		v[i] /= lengthy;
	}
}

/*!
*	\brief	Resets our vector to the 0 values
*   \return none
*/
void Vector4::Zero(void){
	x = 0; y = 0; z = 0;//Set to 0
	w = 0;//Set to default
}