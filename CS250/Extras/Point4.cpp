/*!
*	\file		Point4.cpp
*	\brief		Function definitions for the Math Assignment.
*	\details	Performs the calculations needed for Matrices.
*	\author		Aitor Rodriguez - aitor.r@digipen.edu
*	\date		13/01/2020
*	\copyright	Copyright DigiPen Institute Of Technology. All Rights Reserved
*
*/
#include "Point4.h"
#include "MathUtilities.h"
#define Max 4

/*!
*	\brief	Default constructs a point of 4 parameters
*   \return no return
*/
Point4::Point4(void){
	x = 0; y = 0; z = 0;//Set the first 3 to 0
	w = 1;//Set the w to the default for points
}

/*!
*	\brief	Constructs a new point based on input
*	\param	rhs		Point to use as base
*   \return no return
*/
Point4::Point4(const Point4& rhs){
	for(unsigned int i = 0; i<Max; i++){//For loop to set our values
		v[i] =rhs.v[i];
	}
}

/*!
*	\brief	Constructs a new point based on input
*	\param	xx	    x value of point
*	\param	yy	    y value of point
*	\param	zz	    z value of point
*	\param	ww	    w value of point
*   \return no return
*/
Point4::Point4(float xx, float yy, float zz, float ww){
	//Sets values one by one by hand
	x = xx; y = yy; z = zz;
	w = ww;
}

/*!
*	\brief	Changes our point based on input
*	\param	rhs		point to use as base
*   \return Our changed point (this)
*/
Point4& Point4::operator=(const Point4& rhs){
	//For loop to change values
	for(unsigned int i = 0; i<Max; i++){
		v[i] = rhs.v[i];
	}

	return *this;
}

/*!
*	\brief	Unary negation to the point
*   \return The unary negated point
*/
Point4 Point4::operator-(void) const{
	Point4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = -v[i];
	}

	return Res;
}

/*!
*	\brief	Substraction between points
*	\param	rhs		point to use
*   \return Result vector
*/
Vector4 Point4::operator-(const Point4& rhs) const{
	Vector4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = v[i]-rhs.v[i];
	}

	return Res;
}
/*!
*	\brief	Addition between point and vector
*	\param	rhs		Vector to use
*   \return Result point
*/
Point4 Point4::operator+ (const Vector4& rhs) const{
	Point4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = v[i] + rhs.v[i];
	}

	return Res;
}
/*!
*	\brief	Substraction between point and vector
*	\param	rhs		Vector to use
*   \return Result point
*/
Point4 Point4::operator- (const Vector4& rhs) const{
	Point4 Res;//Variable to return
	
	//For loop to calculate values
	for(unsigned int i = 0; i<Max; i++){
		Res.v[i] = v[i] - rhs.v[i];
	}

	return Res;
}

/*!
*	\brief	addition between point and vector changing ours
*	\param	rhs		point to use
*   \return Our altered point
*/
Point4& Point4::operator+=(const Vector4& rhs){
	Point4 Use = *this;	//Create variable with our point
	*this = (Use + rhs);//Overwrite our point with math
	return *this;
}
/*!
*	\brief	substraction between point and vector changing ours
*	\param	rhs		Vector to use
*   \return Our altered vector
*/
Point4& Point4::operator-=(const Vector4& rhs){
	Point4 Use = *this;	//Create variable with our point
	*this = (Use - rhs);//Overwrite our point with math
	return *this;
}

/*!
*	\brief	comparation between 2 points
*	\param	rhs		point to use
*   \return true or false
*/
bool Point4::operator==(const Point4& rhs) const{
	for(unsigned int i=0; i<Max; i++){//for to check all the parameters
		if(!isEqual(v[i], rhs.v[i]))//If it isn't equal, return false
			return false;
	}
	return true;
}
/*!
*	\brief	inversed comparation between 2 points
*	\param	rhs		point to use
*   \return true or false
*/
bool Point4::operator!=(const Point4& rhs) const{
	return !(*this == rhs);//All went well
}

/*!
*	\brief	Resets our point to the 0 values
*   \return none
*/
void Point4::Zero(void){
	x = 0; y = 0; z = 0;//Set the first 3 to 0
	w = 1;//Set the w to the default for points
}