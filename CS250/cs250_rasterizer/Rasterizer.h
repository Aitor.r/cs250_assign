/*!
******************************************************************************
\file    Rasterizer.h
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  Functions to be called by the Drawing stage, in a .h

******************************************************************************/
#pragma once
#include <math.h>
#include <SFML/Graphics.hpp>//sf::image
#include "Point2D.h"
#include "../Extras/Matrix4.h"

struct FACE {
	int indices[3];
};

//Draws the line between 2 points with the color
void DrawMidpointLine(const Point2D & p1, const Point2D & p2);

//Draws a triangle with the 3 given points
void DrawTriangleSolid(const Point2D & p0, const Point2D & p1, const Point2D & p2);

std::vector<Point2D> Clip(Vector4 plane, Point2D v0, Point2D v1, Point2D v2);

//Wrappers to call the above using a vector of points and a vector of bools to see if they are good to go
void RasterizeTriangle(const std::vector<Point4> vertices, const std::vector<bool> checker, std::vector<Point4> Colo, const std::vector<bool> face, const std::vector<FACE> facer);
void RasterizeLine(const std::vector<Point4> vertices, const std::vector<bool> checker, std::vector<Point4> Colo, const std::vector<bool> face, const std::vector<FACE> facer);

bool Checking(Vector4 plane, Point4 Vert);