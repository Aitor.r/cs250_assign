/*!
******************************************************************************
\file    rasterizer.cpp
\author  Aitor Rodriguez
\par     DP email: aitor.r@digipen.edu
\par     Course: CS250
\par     assignment 2
\date    09-02-2020

\brief
  rasterizer provided by you(Added a helper function and wrappers)

******************************************************************************/
#include "Rasterizer.h"
#include "../GeneralCode/FrameBuffer.h"
#include "../cs250_parser/CS250Parser.h"
#define Size 6
#define ex 0
#define why 1
#define zet 2
#define red 3
#define blu 5
#define gre 4



int Ceiling(float f)
{
    int i = static_cast<int>(f);

    if (static_cast<float>(i) == f)
        return i;

    return i + 1;
}

//Function that rounds. I wanted one because last time Thomas told us not to use round, even though I have used that in the drawtriangle...
int Round(float f)
{
	int i = static_cast<int>(f);
	float k = f-i;
	if ((f - k) >= (0.5f)) {
		return i+1;
	}
    return i;
}

//Updated for z values
void DrawMidpointLine(const Point2D & p1, const Point2D & p2)
{
	
	sf::Color colo(static_cast<unsigned char>(round(p1.r)), static_cast<unsigned char>(round(p1.g)), static_cast<unsigned char>(round(p1.b)));

    int x = Round(p1.x);
    int y = Round(p1.y);
	float z = (p1.z);

    int dx    = Round(p2.x) - x;
    int dy    = Round(p2.y) - y;
	float dz    = (p2.z) - z;
	int xStep = 1, yStep = 1;
	float zStep = (dz);

    if (dx < 0)
    {
        xStep = -1;
        dx    = -dx;
    }
    if (dy < 0)
    {
        yStep = -1;
        dy    = -dy;
    }
	if (dz < 0) {
		zStep = -zStep;
		dz = -dz;
	}

    FB->SetPixel(x, y, z, colo);

    if (abs(dy) > abs(dx)) // |m|>1
    {
        int dstart, dn, dne;
        dstart = 2 * dx - dy;
        dn     = dx;
        dne    = dx - dy;
		zStep = zStep / dy;

        while (dy--)
        {
            y += yStep;
			z += zStep;

            if (dstart > 0)
            {
                dstart += dne;
                x += xStep;
            }
            else
                dstart += dn;

			FB->SetPixel(x, y, z, colo);
        }
    }
    else // |m|<1
    {
        int dstart, de, dne;
        dstart = 2 * dy - dx;
        de     = dy;
        dne    = dy - dx;
		zStep = zStep / dx;

        while (dx--)
        {
            x += xStep;

            if (dstart > 0)
            {
                dstart += dne;
                y += yStep;
				z += zStep;
            }
            else
                dstart += de;

			FB->SetPixel(x, y, z, colo);
        }
    }
}

void DrawTriangleSolid(const Point2D & p0, const Point2D & p1, const Point2D & p2)
{
	std::cout << "Doing" << std::endl;
    // Select TOP, MIDDLE and BOTTOM vertices
    // --------------------------------------
    const Point2D *top, *middle, *bottom;
    bool           middle_is_left = false;
	sf::Color colo;	

    if (p0.val[1] < p1.val[1]) // case 4, 5, or 6
    {
        if (p2.val[1] < p0.val[1]) // case 4
        {
            top            = &p2;
            middle         = &p0;
            bottom         = &p1;
            middle_is_left = true;
        }
        else
        {
            top = &p0;
            if (p1.val[1] < p2.val[1]) // case 5
            {
                middle         = &p1;
                bottom         = &p2;
                middle_is_left = true;
            }
            else // case 6
            {
                middle         = &p2;
                bottom         = &p1;
                middle_is_left = false;
            }
        }
    }
    else // case 1, 2 or 3
    {
        if (p2.val[1] < p1.val[1]) // case 2
        {
            top    = &p2;
            middle = &p1;

            bottom         = &p0;
            middle_is_left = false;
        }
        else
        {
            top = &p1;
            if (p0.val[1] < p2.val[1]) // case 3
            {
                middle         = &p0;
                bottom         = &p2;
                middle_is_left = false;
            }
            else // case 1
            {
                middle         = &p2;
                bottom         = &p0;
                middle_is_left = true;
            }
        }
    }

    // TOP to MIDDLE
    // -------------
    // Select LEFT and RIGHT edge endpoints to use from the top to the middle
    // ----------------------------------------------------------------------
    const Point2D *left, *right;
    if (middle_is_left)
    {
        left  = middle;
        right = bottom;
    }
    else
    {
        right = middle;
        left  = bottom;
    }

    // Calculate the x increments along the LEFT and RIGHT edges (the inverse of the slope
    float xIncLeft  = (left->val[0] - top->val[0]) / (left->val[1] - top->val[1]);
    float xIncRight = (right->val[0] - top->val[0]) / (right->val[1] - top->val[1]);

    // Initial values
	float   z    = (top->val[2]);
	int   y    = Ceiling(top->val[1]);
	int   yMax = Ceiling(middle->val[1]) - 1;
	//int   zMax = Ceiling(top->val[2]) - 1;
    float xL   = top->val[0]; // xL MUST ALWAYS be a double, the ceiling is applied to x and xMax
    float xR   = top->val[0]; // xL MUST ALWAYS be a double, the ceiling is applied to x and xMax
    int   x;
    int   xMax = Ceiling(xR) - 1;

    // Initial Color Interpolation (Plane Equation Parameters)
    // RED
    float v1R[3] = {middle->val[0] - top->val[0], middle->val[1] - top->val[1], static_cast<float>(middle->col[0] - top->col[0])};
    float v2R[3] = {bottom->val[0] - top->val[0], bottom->val[1] - top->val[1], static_cast<float>(bottom->col[0] - top->col[0])};
    // GREEN
    float v1G[3] = {middle->val[0] - top->val[0], middle->val[1] - top->val[1], static_cast<float>(middle->col[1] - top->col[1])};
    float v2G[3] = {bottom->val[0] - top->val[0], bottom->val[1] - top->val[1], static_cast<float>(bottom->col[1] - top->col[1])};
    // BLUE
    float v1B[3] = {middle->val[0] - top->val[0], middle->val[1] - top->val[1], static_cast<float>(middle->col[2] - top->col[2])};
    float v2B[3] = {bottom->val[0] - top->val[0], bottom->val[1] - top->val[1], static_cast<float>(bottom->col[2] - top->col[2])};
    // NORMALS
    float nR[3] = {v1R[1] * v2R[2] - v1R[2] * v2R[1], v1R[2] * v2R[0] - v1R[0] * v2R[2], v1R[0] * v2R[1] - v1R[1] * v2R[0]};
    float nG[3] = {v1G[1] * v2G[2] - v1G[2] * v2G[1], v1G[2] * v2G[0] - v1G[0] * v2G[2], v1G[0] * v2G[1] - v1G[1] * v2G[0]};
    float nB[3] = {v1B[1] * v2B[2] - v1B[2] * v2B[1], v1B[2] * v2B[0] - v1B[0] * v2B[2], v1B[0] * v2B[1] - v1B[1] * v2B[0]};
	float nZ[3] = {v1R[1]*(bottom->val[3]-middle->val[3]) - v2R[1]*(middle->val[3] - top->val[3]),
		(middle->val[3] - top->val[3])*v1R[0] - (bottom->val[3] - middle->val[3])*v2R[0], 
		v1R[0] * v2R[1] - v1R[1] * v2R[0] };
    // Increments
    float rIncX = -nR[0] / nR[2];
    float rIncY = -nR[1] / nR[2];
    float gIncX = -nG[0] / nG[2];
    float gIncY = -nG[1] / nG[2];
    float bIncX = -nB[0] / nB[2];
    float bIncY = -nB[1] / nB[2];
	float IncZx = -nZ[0]/ nZ[2];
	float IncZy = 0.0f;
	if(middle_is_left)
		IncZy = -((nZ[0]*(-xIncLeft))+nZ[1])/nZ[2];
	else
		IncZy = -((nZ[0] * (-xIncRight)) + nZ[1]) / nZ[2];


    float rL = top->col[0];
    float gL = top->col[1];
    float bL = top->col[2];

    float r, g, b;

    // Start the loop, from the y_top to y_middle
    while (y <= yMax)
    {
        // Loop along the scanline, from left to right
        x    = Ceiling(xL);
        xMax = Ceiling(xR) - 1;

        r = rL;
        g = gL;
        b = bL;

		float zetty = (z);

        while (x <= xMax)
        {
			colo.r = static_cast<unsigned char>(r);
			colo.g = static_cast<unsigned char>(g);
			colo.b = static_cast<unsigned char>(b);

			FB->SetPixel(x, y, zetty, colo);

            ++x;
			zetty += IncZx;

            r += rIncX;
            g += gIncX;
            b += bIncX;
        }

        xL += xIncLeft;
        xR += xIncRight;
        ++y;
		z += IncZy;

        rL += rIncY + rIncX * xIncLeft;
        gL += gIncY + gIncX * xIncLeft;
        bL += bIncY + bIncX * xIncLeft;
    }

    // MIDDLE to BOTTOM
    // ----------------
    // Select LEFT and RIGHT edge endpoints to use from the top to the middle
    // ----------------------------------------------------------------------
    // Initial values
    y    = Ceiling(middle->val[1]);
    yMax = Ceiling(bottom->val[1]) - 1;

    if (middle_is_left)
    {
        xIncLeft = (bottom->val[0] - left->val[0]) / (bottom->val[1] - left->val[1]);
        xL       = left->val[0];

		z = left->val[2];
		IncZy = -((nZ[0] * (-xIncLeft)) + nZ[1]) / nZ[2];

        rL = middle->col[0];
        gL = middle->col[1];
        bL = middle->col[2];
    }
    else
    {
        xIncRight = (bottom->val[0] - right->val[0]) / (bottom->val[1] - right->val[1]);
        xR        = right->val[0];
    }

    // Start the loop, from the y_top to y_middle
    while (y <= yMax)
    {
        x    = Ceiling(xL);
        xMax = Ceiling(xR) - 1;

        r = rL;
        g = gL;
        b = bL;

		float zetty = (z);

        // Loop along the scanline, from left to right
        while (x <= xMax)
        {
			colo.r = static_cast<unsigned char>(r);
			colo.g = static_cast<unsigned char>(g);
			colo.b = static_cast<unsigned char>(b);

			FB->SetPixel(x, y, zetty, colo);//Set the pixel taking the z into account

            ++x;
			zetty += IncZx;

            r += rIncX;
            g += gIncX;
            b += bIncX;
        }

        xL += xIncLeft;
        xR += xIncRight;
        ++y;
		z += IncZy;

        rL += rIncY + rIncX * xIncLeft;//Increase the attributes according to their respective formulas
        gL += gIncY + gIncX * xIncLeft;//Increase the attributes according to their respective formulas
        bL += bIncY + bIncX * xIncLeft;//Increase the attributes according to their respective formulas
    }
}

//For culling
bool Checking(Vector4 plane, Point4 Vert) {
	float dp = plane.x * Vert.x + plane.y * Vert.y + plane.z*Vert.z + plane.w;

	if (dp <= 0)
		return true;

	return false;
}

//Helper
bool Checking(Vector4 plane, Point2D Vert) {
	float dp = plane.x * Vert.x + plane.y * Vert.y + plane.z*Vert.z + plane.w;

	if (dp <= 0)
		return true;

	return false;
}

Point2D Intersect(Vector4 plane, Point2D Vert1, Point2D Vert2) {
	//I wanted to avoid repeating computations
	float arra[Size];//6 components
	arra[ex] = Vert2.x - Vert1.x;//x difference
	arra[why] = Vert2.y - Vert1.y;//y difference
	arra[zet] = Vert2.z - Vert1.z;//z difference
	arra[red] = Vert2.r - Vert1.r;//red difference
	arra[gre] = Vert2.g - Vert1.g;//green difference
	arra[blu] = Vert2.b - Vert1.b;//blue difference


	float t = -(plane.x*Vert1.x +plane.y*Vert1.y +plane.z*Vert1.z +plane.w) / (plane.x*(arra[ex])+ plane.y*(arra[why])+ plane.z*(arra[zet]));


	Point2D Res = Point2D(Vert1.x +t*arra[ex], Vert1.y + t* arra[why], Vert1.z + t* arra[zet], 
						  Vert1.r + t*arra[red], Vert1.g+t* arra[gre], Vert1.b+ t*arra[blu]);

	return Res;
}

std::vector<Point2D> Clip(Vector4 plane, Point2D v0, Point2D v1, Point2D v2) {
	std::vector<Point2D> Res;

	if ((Checking(plane, v0)) && (Checking(plane, v1))) {
		Res.push_back(v1);
	}
	else if ((Checking(plane, v0)) && !(Checking(plane, v1))) {
		Res.push_back(Intersect(plane, v0, v1));
	}
	else if (!(Checking(plane, v0)) && (Checking(plane, v1))) {
		Res.push_back(Intersect(plane, v0, v1));
		Res.push_back(v1);
	}

	if ((Checking(plane, v1)) && (Checking(plane, v2))) {
		Res.push_back(v2);
	}
	else if ((Checking(plane, v1)) && !(Checking(plane, v2))) {
		Res.push_back(Intersect(plane, v1, v2));
	}
	else if (!(Checking(plane, v1)) && (Checking(plane, v2))) {
		Res.push_back(Intersect(plane, v1, v2));
		Res.push_back(v2);
	}

	if ((Checking(plane, v2)) && (Checking(plane, v0))) {
		Res.push_back(v0);
	}
	else if ((Checking(plane, v2)) && !(Checking(plane, v0))) {
		Res.push_back(Intersect(plane, v2, v0));
	}
	else if (!(Checking(plane, v2)) && (Checking(plane, v0))) {
		Res.push_back(Intersect(plane, v2, v0));
		Res.push_back(v0);
	}

	return Res;
}

//Draws triangles with given points and bools
void RasterizeTriangle(const std::vector<Point4> vertices, const std::vector<bool> checker, std::vector<Point4> Colo, const std::vector<bool> face, const std::vector<FACE> facer) {

	for (unsigned int i = 0; i < facer.size(); i++) {
		if ((face[i]))
			continue;
		
		if (!checker[facer[i].indices[0]] || !checker[facer[i].indices[1]] ||!checker[facer[i].indices[2]])
			continue;

		Point2D Ver1 = Point2D(vertices[facer[i].indices[0]].x, vertices[facer[i].indices[0]].y, vertices[facer[i].indices[0]].z, Colo[i].x, Colo[i].y, Colo[i].z);
		Point2D Ver2 = Point2D(vertices[facer[i].indices[1]].x, vertices[facer[i].indices[1]].y, vertices[facer[i].indices[1]].z, Colo[i].x, Colo[i].y, Colo[i].z);
		Point2D Ver3 = Point2D(vertices[facer[i].indices[2]].x, vertices[facer[i].indices[2]].y, vertices[facer[i].indices[2]].z, Colo[i].x, Colo[i].y, Colo[i].z);


		DrawTriangleSolid(Ver1, Ver2, Ver3);

	}
}

//Draws Lines with given points and bools
void RasterizeLine(const std::vector<Point4> vertices, const std::vector<bool> checker, std::vector<Point4> Colo, const std::vector<bool> face, const std::vector<FACE> facer) {

	for (unsigned int i = 0; i < CS250Parser::faces.size(); i++) {
		if ((face[i]))
			continue;

		if (!checker[facer[i].indices[0]] || !checker[facer[i].indices[1]] || !checker[facer[i].indices[2]])
			continue;

		Point2D Ver1 = Point2D(vertices[facer[i].indices[0]].x, vertices[facer[i].indices[0]].y, vertices[facer[i].indices[0]].z, Colo[i].x, Colo[i].y, Colo[i].z);
		Point2D Ver2 = Point2D(vertices[facer[i].indices[1]].x, vertices[facer[i].indices[1]].y, vertices[facer[i].indices[1]].z, Colo[i].x, Colo[i].y, Colo[i].z);
		Point2D Ver3 = Point2D(vertices[facer[i].indices[2]].x, vertices[facer[i].indices[2]].y, vertices[facer[i].indices[2]].z, Colo[i].x, Colo[i].y, Colo[i].z);


		DrawMidpointLine(Ver1, Ver2);
		DrawMidpointLine(Ver2, Ver3);
		DrawMidpointLine(Ver3, Ver1);

	}
}
